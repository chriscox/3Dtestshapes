//
//  objects.cpp
//
//  Copyright © 2016-2023 Chris Cox. All rights reserved.
//  Distributed under the MIT License
//

#include <cmath>
#include <climits>
#include "utilities.h"
#include "geometry.h"
#include "objects.h"

/*********************************************************************/

polygon ThinRectangleObject()
{
    polygon result;

    result.reserve(4);
    result.push_back( point2D( 0.5f,  0.05f) );            // 10 cm by 1 cm
    result.push_back( point2D(-0.5f,  0.05f) );
    result.push_back( point2D(-0.5f, -0.05f) );
    result.push_back( point2D( 0.5f, -0.05f) );
    return result;
}

/*********************************************************************/

polygon SquareObject( const float radius )
{
    polygon result;

    result.reserve(4);
    result.push_back( point2D( radius,  radius) );
    result.push_back( point2D(-radius,  radius) );
    result.push_back( point2D(-radius, -radius) );
    result.push_back( point2D( radius, -radius) );
    return result;
}

/*********************************************************************/

polygon RectangleObject( const float radius, const float aspectRatio )
{
    polygon result;
    result.reserve(4);
    float height = radius * aspectRatio;
    result.push_back( point2D( radius,  height) );
    result.push_back( point2D(-radius,  height) );
    result.push_back( point2D(-radius, -height) );
    result.push_back( point2D( radius, -height) );
    return result;
}

/*********************************************************************/

polygon NsidedObject( const float radius, const int Nsides )
{
    polygon result;
    result.reserve((size_t)Nsides);
    //float angleStep = (2.0 * M_PI) / Nsides;
    
    for (int i = 0; i < Nsides; ++i)
        {
        float angle = (float)(i * (2.0 * M_PI) / Nsides);
        float xx = radius * cosf( angle );
        float yy = radius * sinf( angle );
        result.push_back( point2D( xx, yy ) );
        }
    
    return result;
}

/*********************************************************************/

polygon NsidedEllipseObject( const float radius, const int Nsides, const float aspectRatio )
{
    polygon result;
    result.reserve((size_t)Nsides);
    //float angleStep = (2.0 * M_PI) / Nsides;

    float width = radius;
    float height = radius * aspectRatio;
    
    for (int i = 0; i < Nsides; ++i)
        {
        float angle = (float)(i * (2.0 * M_PI) / Nsides);
        float xx = width * cosf( angle );
        float yy = height * sinf( angle );
        result.push_back( point2D( xx, yy ) );
        }
    
    return result;
}
/*********************************************************************/

