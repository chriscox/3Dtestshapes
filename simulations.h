//
//  simulations.h
//
//  Copyright © 2016-2022 Chris Cox. All rights reserved.
//  Distributed under the MIT License
//

#include "config.h"
#include "geometry.h"

/*********************************************************************/

void GenerateAllFiles();

void TwistExtrude( const polygon &object, const char *base_testname );

/*********************************************************************/
