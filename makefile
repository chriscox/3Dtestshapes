#
#    Copyright 2016-2022 Chris Cox
#    Distributed under the MIT License
#

#
# Macros
#

INCLUDE = -I.

CFLAGS = $(INCLUDE) -O3
CPPFLAGS = -std=c++17 $(INCLUDE) -O3

CLIBS = -lm
CPPLIBS = -lm

DEPENDENCYFLAG = -MM


#
# our target programs
#

BINARIES = testshapes


#
# Build rules
#

all : $(BINARIES)

config.o: config.cpp config.h

geometry.o: geometry.cpp geometry.h geometry.hh config.h utilities.hh

objects.o: objects.cpp utilities.h pooled_hashmap.h utilities.hh config.h geometry.hh geometry.h objects.h

simulations.o: simulations.cpp config.h utilities.h pooled_hashmap.h utilities.hh geometry.hh geometry.h objects.h simulations.h

testshapes.o: testshapes.cpp config.h simulations.h geometry.h geometry.hh utilities.hh

utilities.o: utilities.cpp config.h utilities.h pooled_hashmap.h utilities.hh geometry.hh geometry.h

testshapes: config.o geometry.o testshapes.o objects.o simulations.o utilities.o
	$(CXX) $(CPPFLAGS) $^ -o $@



SUFFIXES:
.SUFFIXES: .c .cpp


# declare some targets to be fakes without real dependencies
.PHONY : clean dependencies

# remove all the stuff we build
clean : 
		rm -f *.o a.out $(BINARIES)


# generate dependency listing from all the source files

SOURCES = $(wildcard *.c)  $(wildcard *.cpp)
dependencies :   $(SOURCES)
	$(CXX) $(DEPENDENCYFLAG) $(CPPFLAGS) $^


