//
//  objects.h
//
//  Copyright © 2016-2022 Chris Cox. All rights reserved.
//  Distributed under the MIT License
//

#ifndef OBJECTS_H
#define OBJECTS_H

#include "geometry.hh"

/*********************************************************************/

polygon ThinRectangleObject();

polygon SquareObject( const float radius );

polygon RectangleObject( const float radius, const float aspectRatio );

polygon NsidedObject( const float radius, const int Nsides );

polygon NsidedEllipseObject( const float radius, const int Nsides, const float aspectRatio );

/*********************************************************************/

#endif // OBJECTS_H
