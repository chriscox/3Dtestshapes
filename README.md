# 3D test shapes
Distributed under the MIT License

Generate a variety of 3D files for testing file format code, rendering, compression, etc.\
This is geometry only, all triangles.

## Building

Requires C++17\
You should be able to build using the supplied makefile.

## Running

1. Default settings generate about 12 Gig of files in binary STL and OBJ.
1. Enabling large files (1 - 10 Gig) adds another 92 Gig of files.
1. Enabling Huge files ( > 10 Gig) adds another 675 Gig of files, and needs more then 64 Gig of RAM to run.

## 3D Formats Written

* STL Binary
* STL ASCII
* OBJ ASCII
* X3D TriangleSet
* X3D IndexedFaceSet
* PLY ASCII
* PLY Binary
* AMF mesh
* DAE mesh
* VRML 2.0 IndexedFaceSet
