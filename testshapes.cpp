//
//  main.cpp
//
//  Copyright © 2017-2022 Chris Cox. All rights reserved.
//  Distributed under the MIT License
//

#include <cstdio>
#include <unistd.h>
#include "config.h"
#include "simulations.h"

/*********************************************************************/
/*********************************************************************/

int main(int argc, const char * argv[]) {
    
    parse_arguments( argc, argv );
    
    GenerateAllFiles();
    
    return 0;
}

/*********************************************************************/
/*********************************************************************/
