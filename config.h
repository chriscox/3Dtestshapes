//
//  config.h
//
//  Copyright © 2017-2023 Chris Cox. All rights reserved.
//  Distributed under the MIT License
//


#ifndef CONFIG_H
#define CONFIG_H

#include <cstdio>
#include <cstdlib>
#include <cstdint>

/*********************************************************************/
/*********************************************************************/

// global configuration

const char kVersionString[] = "0.8b";

extern size_t angle_count;
extern float final_angle;
extern float full_height;
extern float diameter;

extern bool outputSVGGeometry;
extern bool outputSTLFile;
extern bool outputSTLBinaryFile;
extern bool outputX3DFile;
extern bool outputX3DIndexFile;
extern bool outputPLYFile;
extern bool outputPLYBinaryFile;
extern bool outputAMFFile;
extern bool outputOBJFile;
extern bool outputDAEFile;
extern bool outputVRMLFile;

extern bool generateSmallFiles;
extern bool generateMediumFiles;
extern bool generateLargeFiles;
extern bool generateHugeFiles;
extern bool generateBenchmark;

/*********************************************************************/
/*********************************************************************/

const float epsilon( 1.0e-9f );

/*********************************************************************/

void print_usage(const char *argv[]);
void parse_arguments( int argc, const char *argv[] );
void SetDefaults();

/*********************************************************************/
/*********************************************************************/

#endif    // CONFIG_H
