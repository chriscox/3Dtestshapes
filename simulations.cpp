//
//  simulations.cpp
//
//  Copyright © 2016-2023 Chris Cox. All rights reserved.
//  Distributed under the MIT License
//

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <climits>
#include <vector>
#include <map>
#include <algorithm>
#include <iostream>
#include <time.h>
#include <cstdint>
#include "config.h"
#include "utilities.h"
#include "geometry.h"
#include "objects.h"
#include "simulations.h"


/*********************************************************************/
/*********************************************************************/

// very thin edges, but a good solid shape
void TwistExtrude( const polygon &object, const char *base_testname )
{
    FILE *svgFile = StartSVGFile( base_testname );
    // optionally save our geometry to an SVG file for verification
    AppendPolygonToSVGFile( svgFile, object );
    
    // i == 0, write out base polygon
    MULTI_3D_FILE objFile = Start3DFile( base_testname, (size_t)(2*object.size()*(angle_count+1)) );
    Write3DFlatPolygon( objFile, object, 0.0, false );
    
    polygon previous_poly = object;
    float previousZ = 0.0;
    
    float final_angle_radians = (float)(final_angle * M_PI / 180.0);

    for ( size_t i = 1; i <= angle_count; ++i )
        {
        float angle = i * final_angle_radians / angle_count;
        float Z = i * full_height / angle_count;

        // rotate shape around origin (center of table)
        polygon current_poly = rotatePolygon( object, angle );        // NOTE - allocates memory

        // optionally save our geometry to an SVG file for verification
        AppendPolygonToSVGFile( svgFile, current_poly );
        
        // i > 0, write out last polygon to current polygon surfaces
        Write3DConnectingEdges( objFile, previous_poly, current_poly, previousZ, Z );
        
        previous_poly = current_poly;
        previousZ = Z;
        
        }    // per rotation loop
    
    // i == angle_count, write out final rotated polygon
    Write3DFlatPolygon( objFile, previous_poly, full_height, true );
    
    // finish up the shape files
    FinishSVGFile( svgFile );
    Finish3DFile( objFile );
}

/*********************************************************************/
/*********************************************************************/

void SquareTest()
{
    const char testname[] = "Square";
    
    // initialize test geometry
    polygon object = SquareObject( diameter/2.0f );
    
    TwistExtrude( object, testname );
}

/*********************************************************************/

void SquareOffsetTest()
{
    const char testname[] = "SquareOffset";
    
    // initialize test geometry
    polygon object = SquareObject( diameter/2.0f );
    object = offsetPolygon( object, point2D( 0.0f, diameter/4.0f) );
    
    TwistExtrude( object, testname );
}

/*********************************************************************/

void EllipseTest()
{
    const char testname[] = "Ellipse";
    
    // initialize test geometry
    polygon object = NsidedEllipseObject( diameter/2.0f, 128, 0.25 );
    
    TwistExtrude( object, testname );
}

/*********************************************************************/

void EllipseOffsetTest()
{
    const char testname[] = "EllipseOffset";
    
    // initialize test geometry
    polygon object = NsidedEllipseObject( diameter/2.0f, 128, 0.25 );
    object = offsetPolygon( object, point2D( diameter/4.0f, diameter/6.0f ) );
    
    TwistExtrude( object, testname );
}

/*********************************************************************/

void TriangleTest()
{
    const char testname[] = "Triangle";
    
    // initialize test geometry
    polygon object = NsidedObject( diameter/2.0f, 3 );
    
    TwistExtrude( object, testname );
}

/*********************************************************************/

void TriangleOffsetTest()
{
    const char testname[] = "TriangleOffset";
    
    // initialize test geometry
    polygon object = NsidedObject( diameter/2.0f, 3 );
    object = offsetPolygon( object, point2D( 0.0f, diameter/4.0f) );
    
    TwistExtrude( object, testname );
}

/*********************************************************************/
/*********************************************************************/

void WriteTessellatedCube( MULTI_3D_FILE &out, float x, float y, float z, float size, const size_t divisions )
{
    point3D base( x, y, z );
    point3D baseX( x+size, y, z );
    point3D baseY( x, y+size, z );
    point3D baseXY( x+size, y+size, z );
    
    point3D topZ( x, y, z+size );
    point3D topX( x+size, y, z+size );
    point3D topY( x, y+size, z+size );
    point3D topXY (x+size, y+size, z+size);
    
    Write3DTessellatedRectangle( out, divisions, base, baseY, baseX );      // bottom - turned to get correct point order
    Write3DTessellatedRectangle( out, divisions, base, baseX, topZ );       // XZ front
    Write3DTessellatedRectangle( out, divisions, baseX, baseXY, topX );     // YZ back
    Write3DTessellatedRectangle( out, divisions, baseXY, baseY, topXY );    // XZ back
    Write3DTessellatedRectangle( out, divisions, baseY, base, topY );       // YZ front
    Write3DTessellatedRectangle( out, divisions, topZ, topX, topY );        // top
}

/*********************************************************************/

void WriteLoneTriangle( MULTI_3D_FILE &out, float x, float y, float z, float size )
{
    polygon base;
    point3D p1( x, y, z );
    point3D p2( x+size, y, z );
    point3D p3( x+size, y+size, z );

    SaveTriangle( out, p2, p1, p3 ); // order doesn't matter, normal doesn't really matter
}

/*********************************************************************/

void WriteRectangle( MULTI_3D_FILE &out, float x, float y, float z, float size )
{
    polygon base;
    base.push_back( point2D( x, y ) );
    base.push_back( point2D( x + size, y ) );
    base.push_back( point2D( x + size, y + size ) );
    base.push_back( point2D( x, y + size ) );
    
    Write3DFlatRectangle( out, base, z, false );
}

/*********************************************************************/

void WriteCube( MULTI_3D_FILE &out, float x, float y, float z, float size )
{
    polygon base;
    base.push_back( point2D( x, y ) );
    base.push_back( point2D( x + size, y ) );
    base.push_back( point2D( x + size, y + size ) );
    base.push_back( point2D( x, y + size ) );
    
    Write3DFlatRectangle( out, base, z, false );
    Write3DConnectingEdges( out, base, base, z, z+size );
    Write3DFlatRectangle( out, base, z+size, true );
}

/*********************************************************************/

/// Cubicle - just the walls, no floor or ceiling
void WriteOpenCube( MULTI_3D_FILE &out, float x, float y, float z, float size )
{
    polygon base;
    base.push_back( point2D( x, y ) );
    base.push_back( point2D( x + size, y ) );
    base.push_back( point2D( x + size, y + size ) );
    base.push_back( point2D( x, y + size ) );
    
    Write3DConnectingEdges( out, base, base, z, z+size );
}

/*********************************************************************/

void OpenCube(float size)
{
    const size_t namelimit = 1024;
    char tempname[namelimit];
    const char testname[] = "OpenCube";

    snprintf(tempname,namelimit,"%s",testname);

    MULTI_3D_FILE objFile = Start3DFile( tempname, 8 );

    WriteOpenCube( objFile, 0.0f, 0.0f, 0.0f, size );

    Finish3DFile( objFile );
}

/*********************************************************************/

void CubeOfOpenCubes(size_t count)
{
    const size_t namelimit = 1024;
    char tempname[namelimit];
    const char testname[] = "CubeOfOpenCubes";
    
    const size_t limit = count;    // 8 * 100^3 = 8 million triangles
    const float fraction = 0.9f;
    
    snprintf(tempname,namelimit,"%s_%lu",testname,count);
    
    MULTI_3D_FILE objFile = Start3DFile( tempname, 12ULL*limit*limit*limit );
    
    for (size_t z = 0; z < limit; ++z)
    for (size_t y = 0; y < limit; ++y)
    for (size_t x = 0; x < limit; ++x)
        {
        // 4 faces,2 triangles each = 8 triangles per cube written
        WriteOpenCube( objFile, x, y, z, fraction );
        }

    Finish3DFile( objFile );
}

/*********************************************************************/

void TessellatedCube( float size, size_t divisions )
{
    const size_t namelimit = 1024;
    char tempname[namelimit];
    const char testname[] = "TessellatedCube";

    snprintf(tempname,namelimit,"%s_%d",testname,(int)divisions);
    
    MULTI_3D_FILE objFile = Start3DFile( tempname, 12ULL*divisions*divisions );
    
    WriteTessellatedCube( objFile, 0.0f, 0.0f, 0.0f, size, divisions );

    // finish up this run
    Finish3DFile( objFile );
}

/*********************************************************************/

void Triangle( float size )
{
    const size_t namelimit = 1024;
    char tempname[namelimit];
    const char testname[] = "SingleTriangle";

    snprintf(tempname,namelimit,"%s",testname);
    
    MULTI_3D_FILE objFile = Start3DFile( tempname, 1 );
    
    WriteLoneTriangle( objFile, 0.0f, 0.0f, 0.0f, size );

    // finish up this run
    Finish3DFile( objFile );
}

/*********************************************************************/

void Rectangle( float size )
{
    const size_t namelimit = 1024;
    char tempname[namelimit];
    const char testname[] = "SingleRectangle";

    snprintf(tempname,namelimit,"%s",testname);
    
    MULTI_3D_FILE objFile = Start3DFile( tempname, 2 );
    
    WriteRectangle( objFile, 0.0f, 0.0f, 0.0f, size );

    // finish up this run
    Finish3DFile( objFile );
}

/*********************************************************************/

void CubeOfCubes(size_t count)
{
    const size_t namelimit = 1024;
    char tempname[namelimit];
    const char testname[] = "CubeOfCubes";
    
    const size_t limit = count;    // 12 * 100^3 = 12 million triangles
    const float fraction = 0.9f;
    
    snprintf(tempname,namelimit,"%s_%lu",testname,count);
    
    MULTI_3D_FILE objFile = Start3DFile( tempname, 12ULL*count*count*count );
    
    for (size_t z = 0; z < limit; ++z)
    for (size_t y = 0; y < limit; ++y)
    for (size_t x = 0; x < limit; ++x)
        {
        // 6 faces,2 triangles each = 12 triangles per cube written
        WriteCube( objFile, x, y, z, fraction );
        }

    Finish3DFile( objFile );
}

/*********************************************************************/

void CubeOfCubesJittered(size_t count)
{
    const size_t namelimit = 1024;
    char tempname[namelimit];
    const char testname[] = "CubeOfCubesJittered";
    
    const size_t limit = count;    // 12 * 100^3 = 12 million triangles
    const float fraction = 0.8f;    // radius
    const float jitter = 0.17f;
    
    srandom(42);
    
    snprintf(tempname,namelimit,"%s_%lu",testname,count);
    
    MULTI_3D_FILE objFile = Start3DFile( tempname, 12ULL*count*count*count );
    
    for (size_t z = 0; z < limit; ++z)
    for (size_t y = 0; y < limit; ++y)
    for (size_t x = 0; x < limit; ++x)
        {
        point3D center = point3D(x,y,z);
        center = center + Jitter3D( jitter );
        WriteCube( objFile, center.x, center.y, center.z, fraction );
        }

    Finish3DFile( objFile );
}

/*********************************************************************/

void CheckerCubes(size_t count, float fraction = 0.9f )
{
    const size_t namelimit = 1024;
    char tempname[namelimit];
    const char testname[] = "CheckerCubes";
    
    const size_t limit = count;    // 12 * 100^3 = 12 million triangles
    
    snprintf(tempname,namelimit,"%s_%lu_%g",testname,count,fraction);
    
    MULTI_3D_FILE objFile = Start3DFile( tempname, 6ULL*count*count*count + 3*count*count );    // half the count of full cubes, plus odd edges
    
    for (size_t z = 0; z < limit; ++z)
    for (size_t y = 0; y < limit; ++y)
    for (size_t x = 0; x < limit; ++x)
        {
        
        if ( ((x ^ y ^ z) & 1) == 1)
            continue;
        
        // 6 faces,2 triangles each = 12 triangles per cube written
        WriteCube( objFile, x, y, z, fraction );
        }

    Finish3DFile( objFile );
}

/*********************************************************************/

void SphereOfCubes(size_t count)
{
    const size_t namelimit = 1024;
    char tempname[namelimit];
    const char testname[] = "SphereOfCubes";
    
    const size_t limit = count;
    float radius2 = 0.25f * (float)limit * (float)limit;
    const float fraction = 0.9f;
    
    snprintf(tempname,namelimit,"%s_%lu",testname,count);
    
    MULTI_3D_FILE objFile = Start3DFile( tempname, 9ULL*limit*limit*limit );   // ~3/4 filled
    
    for (size_t z = 0; z < limit; ++z)
    for (size_t y = 0; y < limit; ++y)
    for (size_t x = 0; x < limit; ++x)
        {
        float fx = (float)(x - limit/2 + 0.5);
        float fy = (float)(y - limit/2 + 0.5);
        float fz = (float)(z - limit/2 + 0.5);
        float dist2 = fx*fx + fy*fy + fz*fz;
        
        if (dist2 > radius2)
            continue;
        
        WriteCube( objFile, x, y, z, fraction );
        }

    Finish3DFile( objFile );
}

/*********************************************************************/

void SaveTriangleList( MULTI_3D_FILE &out, const triangle_list triangles )
{
    for (triangle_list::const_iterator iter = triangles.begin(); iter != triangles.end(); ++iter)
        SaveTriangle( out, iter->p1, iter->p2, iter->p3 );
}

/*********************************************************************/

/*
Each subdivision replaces 1 triangle with 4 triangles.
*/
void subdivideFlat( const point3D &v1, const point3D &v2, const point3D &v3,
                triangle_list &out_triangles, const size_t depth )
{
    if(depth == 0) {
        triangle temp(v1,v2,v3);
        out_triangles.push_back( temp );
        return;
    }
    
    point3D v12( v1 + v2 );
    point3D v23( v2 + v3 );
    point3D v31( v3 + v1 );
    v12 = v12.scale(0.5);   // average of v1 and v2    v1 + (v2-v1)/2 ==  v1 + v2/2 - v1/2 == v1/2 + v2/2 = (v1+v2)/2
    v23 = v23.scale(0.5);
    v31 = v31.scale(0.5);
    
    subdivideFlat(v1, v12, v31, out_triangles, depth - 1);
    subdivideFlat(v2, v23, v12, out_triangles, depth - 1);
    subdivideFlat(v3, v31, v23, out_triangles, depth - 1);
    subdivideFlat(v12, v23, v31, out_triangles, depth - 1);
}

/*********************************************************************/

void Tetrahedron( triangle_list &out_triangles, const size_t depth )
{
    const float f29 = sqrtf( 2.0f / 9.0f );
    const float f23 = sqrtf( 2.0f / 3.0f );
    const float f13 = -1.0f / 3.0f;
    
    const point3D p1( sqrt(8.0f/9.0f), 0, f13 );
    const point3D p2( -f29, f23, f13 );
    const point3D p3( -f29, -f23, f13 );
    const point3D p4( 0,0,1 );

    subdivideFlat( p1, p3, p2, out_triangles, depth );
    subdivideFlat( p1, p2, p4, out_triangles, depth );
    subdivideFlat( p2, p3, p4, out_triangles, depth );
    subdivideFlat( p3, p1, p4, out_triangles, depth );
}

/*********************************************************************/

// triangles = 4^(depth+1)
void TessellatedTetrahedron(size_t depth)
{
    const size_t namelimit = 1024;
    char tempname[namelimit];
    const char testname[] = "Tetrahedron";
    
    snprintf(tempname,namelimit,"%s_%lu",testname,depth);
    
    MULTI_3D_FILE objFile = Start3DFile( tempname, (size_t)(4ULL*pow(4,depth)) );
    
    triangle_list out_tri;
    
    Tetrahedron( out_tri, depth );
    
    SaveTriangleList( objFile, out_tri );

    Finish3DFile( objFile );
}

/*********************************************************************/

/* 
https://stackoverflow.com/questions/17705621/algorithm-for-a-geodesic-sphere
https://schneide.wordpress.com/2016/07/15/generating-an-icosphere-in-c/
https://www.opengl.org.ru/docs/pg/0208.html

Each subdivision replaces 1 triangle with 4 triangles.
This version works on the surface of a unit sphere due to the normalize calls
*/
void subdivideSpherical( const point3D &v1, const point3D &v2, const point3D &v3,
                triangle_list &sphere_triangles, const size_t depth )
{
    if(depth == 0) {
        triangle temp(v1,v3,v2);
        sphere_triangles.push_back( temp );
        return;
    }
    
    const point3D v12 = normalizeVector( v1 + v2 );
    const point3D v23 = normalizeVector( v2 + v3 );
    const point3D v31 = normalizeVector( v3 + v1 );
    
    subdivideSpherical(v1, v12, v31, sphere_triangles, depth - 1);
    subdivideSpherical(v2, v23, v12, sphere_triangles, depth - 1);
    subdivideSpherical(v3, v31, v23, sphere_triangles, depth - 1);
    subdivideSpherical(v12, v23, v31, sphere_triangles, depth - 1);
}

/*********************************************************************/

/*
    Returns a unit (radius 1) sphere centered on the origin
    total triangles = 20*(4^Depth)
*/
void initialize_sphere( triangle_list &sphere_triangles, const size_t depth )
{
    const float X = 0.525731112119133606f;
    const float Z = 0.850650808352039932f;
    const int starting_triangles = 20;
    
    const point3D points[12] = {
        { -X, 0.0, Z }, { X, 0.0, Z }, { -X, 0.0, -Z }, { X, 0.0, -Z },
        { 0.0, Z, X }, { 0.0, Z, -X }, { 0.0, -Z, X }, { 0.0, -Z, -X },
        { Z, X, 0.0 }, { -Z, X, 0.0 }, { Z, -X, 0.0 }, { -Z, -X, 0.0 }
    };
    
    const int indices[starting_triangles][3] = {
        { 0, 4, 1 }, { 0, 9, 4 }, { 9, 5, 4 }, { 4, 5, 8 }, { 4, 8, 1 },
        { 8, 10, 1 }, { 8, 3, 10 }, { 5, 3, 8 }, { 5, 2, 3 }, { 2, 7, 3 },
        { 7, 10, 3 }, { 7, 6, 10 }, { 7, 11, 6 }, { 11, 0, 6 }, { 0, 1, 6 },
        { 6, 1, 10 }, { 9, 0, 11 }, { 9, 11, 2 }, { 9, 2, 5 }, { 7, 2, 11 }
    };

    for( int i = 0; i < starting_triangles; ++i )
        subdivideSpherical(points[indices[i][0]], points[indices[i][1]], points[indices[i][2]], sphere_triangles, depth);
}

/*********************************************************************/

typedef std::map<size_t, triangle_list> polyCache;

/*********************************************************************/

void WriteSphere( MULTI_3D_FILE &out, point3D center, float radius, size_t quality )
{
    static polyCache sphereStorage;
    
    triangle_list sphere = sphereStorage[quality];
    
    if (sphere.size() == 0)
        {
        // intialize this sphere and store it in our cache
        initialize_sphere( sphere, quality );
        if (quality <= 7)       // cache up to about 4 Meg spheres, don't cache larger ones
            sphereStorage[quality] = sphere;
        }
    
    sphere = scale3D( sphere, radius );
    sphere = offset3D( sphere, center );
    
    // output triangles in sphere
    SaveTriangleList( out, sphere );
}

/*********************************************************************/

void CubeOfSpheres(size_t count, size_t quality)
{
    const size_t namelimit = 1024;
    char tempname[namelimit];
    const char testname[] = "CubeOfSpheres";
    
    const size_t limit = count;
    const float fraction = 0.45f;    // radius
    
    snprintf(tempname,namelimit,"%s_%lu_%lu",testname,count,quality);
    
    MULTI_3D_FILE objFile = Start3DFile( tempname, (size_t)(20ULL*pow(4,quality)*limit*limit*limit) );
    
    for (size_t z = 0; z < limit; ++z)
    for (size_t y = 0; y < limit; ++y)
    for (size_t x = 0; x < limit; ++x)
        {
        WriteSphere( objFile, point3D(x,y,z), fraction, quality );
        }

    Finish3DFile( objFile );
}

/*********************************************************************/

void CubeOfSpheresJittered( size_t count, size_t quality )
{
    const size_t namelimit = 1024;
    char tempname[namelimit];
    const char testname[] = "CubeOfSpheresJittered";
    
    const size_t limit = count;
    const float fraction = 0.4f;    // radius
    const float jitter = 0.15f;
    
    srandom(42);
    
    snprintf(tempname,namelimit,"%s_%lu_%lu",testname,count,quality);
    
    MULTI_3D_FILE objFile = Start3DFile( tempname, (size_t)(20ULL*pow(4,quality)*limit*limit*limit) );
    
    for (size_t z = 0; z < limit; ++z)
    for (size_t y = 0; y < limit; ++y)
    for (size_t x = 0; x < limit; ++x)
        {
        point3D center = point3D(x,y,z);
        center = center + Jitter3D( jitter );
        WriteSphere( objFile, center, fraction, quality );
        }

    Finish3DFile( objFile );
}

/*********************************************************************/

void SphereOfSpheres(size_t count, size_t quality)
{
    const size_t namelimit = 1024;
    char tempname[namelimit];
    const char testname[] = "SphereOfSpheres";
    
    const size_t limit = count;
    float radius2 = 0.25f * (float)limit * (float)limit;
    const float fraction = 0.45f;
    
    snprintf(tempname,namelimit,"%s_%lu_%lu",testname,count,quality);
    
    MULTI_3D_FILE objFile = Start3DFile( tempname, (size_t)(15ULL*pow(4,quality)*limit*limit*limit) );    // ~3/4 filled
    
    for (size_t z = 0; z < limit; ++z)
    for (size_t y = 0; y < limit; ++y)
    for (size_t x = 0; x < limit; ++x)
        {
        float fx = (float)(x - limit/2 + 0.5);
        float fy = (float)(y - limit/2 + 0.5);
        float fz = (float)(z - limit/2 + 0.5);
        float dist2 = fx*fx + fy*fy + fz*fz;
        
        if (dist2 > radius2)
            continue;
        
        WriteSphere( objFile, point3D(x,y,z), fraction, quality );
        }

    Finish3DFile( objFile );
}

/*********************************************************************/

void SimpleSphere(size_t quality)
{
    const size_t namelimit = 1024;
    char tempname[namelimit];
    const char testname[] = "SimpleSphere";
    
    snprintf(tempname,namelimit,"%s_%lu",testname,quality);
    
    MULTI_3D_FILE objFile = Start3DFile( tempname, (size_t)(20ULL*pow(4,quality)) );
    
    WriteSphere( objFile, point3D(0.0,0.0,0.0), 1.0, quality );

    Finish3DFile( objFile );
}

/*********************************************************************/
/*********************************************************************/

// by default, small and medium files generate about 12 Gig of data files (STL binary, OBJ)

void GenerateAllFiles()
{
    clock_t start_time = 1, end_time = 1;

    if (generateBenchmark) {
        generateSmallFiles = true;
        generateMediumFiles = true;
        generateLargeFiles = false;
        generateHugeFiles = false;
        start_time = clock();
    }

// debug and proof of concept, small files
    if (generateSmallFiles)
        {
        Triangle( 1.0 );        // single triangle
        Rectangle( 1.0 );       // single rectangle
        CubeOfCubes(1);         // single cube
        
        CubeOfCubes(2);         // 8 cubes
        CubeOfCubesJittered(2); // 8 cubes with offsets
        
        OpenCube( 10.0 );       // open edges test
        CubeOfOpenCubes(2);     // 8 open cubes
        CubeOfOpenCubes(3);     // 27 open cubes
        CubeOfOpenCubes(4);     // 64 open cubes
        
        SimpleSphere( 2 );      // sphere with 2 subdivisions = 20 * 16 polygons
        CubeOfSpheres(2, 2);    // 8 spheres with 2 subdivisions
        TessellatedCube( 10.0, 4 );   // tessellated cube, 4x4 divisions per side, 16*6*2 triangles
        TessellatedTetrahedron( 1 );    // 4*4 = 16 triangles

        // rotate extrude shapes - repetition with less simplistic structure, small files
        TriangleTest();
        SquareTest();
        EllipseTest();
        
        TriangleOffsetTest();
        SquareOffsetTest();
        EllipseOffsetTest();
        }


// medium test files for format testing and compression (less than 1 Gig)
    if (generateMediumFiles)
        {
        CubeOfCubes(10);                       // 600 KB binary STL, 431 KB OBJ
        CubeOfCubes(20);                       // 4.8 MB binary STL, 3.8 MB OBJ
        CubeOfCubes(50);                       // 75 MB binary STL, 65.4 MB OBJ
        CubeOfCubes(100);                      // 600 MB binary STL, 561 MB OBJ
        CheckerCubes(50);                      // 37.5 MB binary STL, 32.5 MB OBJ
        CheckerCubes(100);                     // 300 MB binary STL, 278 MB OBJ
        CheckerCubes(91, 1.2f);                // 226.1 MB binary STL, 208 MB OBJ

        if (!generateBenchmark)
            {
            CubeOfCubesJittered(100);          // 600 MB binary STL, 561 MB OBJ
            SphereOfCubes(50);                 // 39.5 MB binary STL, 34.3 MB OBJ
            SphereOfCubes(100);                // 314 MB binary STL, 292 MB OBJ
            CubeOfSpheres(30, 2);              // 432 MB binary STL, 354 MB OBJ
            CubeOfSpheresJittered( 30, 2 );    // 432 MB binary STL, 354 MB OBJ
            SphereOfSpheres(30, 2);            // 229 MB binary STL, 185 MB OBJ
            SphereOfSpheres(30, 3);            // 917 MB binary STL, 757 MB OBJ
            SimpleSphere( 8 );                 // 65.5 MB binary STL, 49.5 OBJ
            SimpleSphere( 9 );                 // 262 MB binary STL, 210 MB OBJ
            TessellatedCube( 10.0, 500 );      // 150 MB binary STL, 115.6 MB OBJ
            TessellatedCube( 10.0, 1000 );     // 432 MB binary STL, 482.4 MB OBJ
            TessellatedTetrahedron( 8 );       // 13.1 MB binary STL, 9.4 MB OBJ
            TessellatedTetrahedron( 10 );      // 210 MB binary STL, 167 MB OBJ
            TessellatedTetrahedron( 11 );      // 839 MB binary STL, 687 MB OBJ
            }
        }


// large files for compression and testing (1 to 10 Gig)
// runs decently with 16 Gig of RAM
// generates another 92 Gig of data
    if (generateLargeFiles)
        {
        CubeOfCubes(200);                   // 4.80 GB binary STL, 4.87 GB OBJ
        CheckerCubes(200);                  // 2.40 GB binary STL, 2.41 GB OBJ
        SphereOfCubes(200);                 // 2.51 GB binary STL, 2.53 GB OBJ
        CubeOfCubesJittered(200);           // 4.80 GB binary STL, 4.87 GB OBJ
        CubeOfSpheres(40, 2);               // 1.02 GB binary STL, 852 MB OBJ
        CubeOfSpheres(50, 2);               // 2.0 GB binary STL, 1.73 GB OBJ
        CubeOfSpheres(70, 2);               // 5.49 GB binary STL, 4.87 GB OBJ
        CubeOfSpheres(30, 3);               // 1.73 GB binary STL, 1.47 GB OBJ
        CubeOfSpheres(50, 3);               // 8.0 GB binary STL, 7.1 GB OBJ
        CubeOfSpheres(30, 4);               // 6.91 GB binary STL, 6.1 GB OBJ
        CubeOfSpheresJittered( 30, 3 );     // 1.73 GB binary STL, 1.47 GB OBJ
        CubeOfSpheresJittered( 30, 4 );     // 6.91 GB binary STL, 6.09 GB OBJ
        SphereOfSpheres(30, 4);             // 3.67 GB binary STL, 3.2 GB OBJ
        SimpleSphere( 10 );                 // 1.05 GB binary STL, 861 MB OBJ
        SimpleSphere( 11 );                 // 4.19 GB binary STL, 3.65 GB OBJ
        TessellatedCube( 10.0, 2000 );      // 2.4 GB binary STL, 2.03 GB OBJ
        TessellatedCube( 10.0, 4000 );      // 9.6 GB binary STL, 8.33 GB OBJ
        TessellatedTetrahedron( 12 );       // 3.36 GB binary STL, 2.91 GB OBJ
        }


// HUGE files for compression and testing (over 10 Gig)

// These require at least 64 GB of RAM to avoid swapping badly
//    ...and a lot of time to build, write, and then free data.
// generates another 675 Gig of data

    if (generateHugeFiles)
        {
        CubeOfCubes(400);                   // 38.4 GB binary STL, 41.6 GB OBJ
        CheckerCubes(400);                  // 19.2 GB binary STL, 20.5 GB OBJ
        SphereOfCubes(400);                 // 20.1 GB binary STL, 21.6 GB OBJ
        CubeOfCubesJittered(400);           // 38.4 GB binary STL, 41.6 GB OBJ
        CubeOfSpheres(70, 3);               // 21.9 GB binary STL, 20.3 GB OBJ
        CubeOfSpheres(50, 4);               // 32.0 GB binary STL, 29.9 GB OBJ
        CubeOfSpheres(30, 5);               // 27.6 GB binary STL, 25.6 GB OBJ
        CubeOfSpheresJittered( 50, 4 );     // 32.0 GB binary STL, 29.9 GB OBJ
        SphereOfSpheres(50, 4);             // 16.8 GB binary STL, 15.4 GB OBJ
        SphereOfSpheres(30, 5);             // 27.7 GB binary STL, 25.6 GB OBJ
        SimpleSphere( 12 );                 // 16.8 GB binary STL, 15.2 GB OBJ
        TessellatedCube( 10.0, 8000 );      // 38.4 GB binary STL, 35.2 GB OBJ
        TessellatedTetrahedron( 13 );       // 13.4 GB binary STL, 12.0 GB OBJ
        }


// TODO: excessively huge files, test 32 bit overflows in some formats ?

// TODO: files broken in specific ways???? (that could take a while, and becomes more of a QA/fuzzing tool)



    if (generateBenchmark) {
        end_time = clock();
        double interval = (end_time - start_time)/ (double)(CLOCKS_PER_SEC);
        printf("testshapes benchmark: %g seconds\n", interval );
    }

}

/*********************************************************************/
/*********************************************************************/
