//
//  geometry.h
//
//  Copyright © 2016-2023 Chris Cox. All rights reserved.
//  Distributed under the MIT License
//

#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <cmath>
#include <vector>
#include "geometry.hh"
#include "utilities.hh"

/*********************************************************************/

// rotate counter clockwise
inline point2D rotatePoint( point2D &input, float angle )
{
    point2D result;
    const float ca = cosf(angle);
    const float sa = sinf(angle);
    result.x = ca * input.x - sa * input.y;
    result.y = sa * input.x + ca * input.y;
    return result;
}

// rotate counter clockwise
inline point2D rotatePoint( const point2D &input, const float CosineAngle, const float SineAngle )
{
    point2D result;
    result.x = CosineAngle * input.x -   SineAngle * input.y;
    result.y =   SineAngle * input.x + CosineAngle * input.y;
    return result;
}

inline point2D offsetPoint( const point2D &input, const point2D &offset )
{
    point2D result( input.x + offset.x, input.y + offset.y );
    return result;
}

inline point2D scalePoint( const point2D &input, const float scale )
{
    point2D result( input.x * scale, input.y * scale );
    return result;
}

inline point2D subtractPoints( const point2D &input1, const point2D &input2 )
{
    point2D result( input1.x - input2.x, input1.y - input2.y );
    return result;
}

inline point2D normalizeVector( const point2D &input )
{
    const float epsilonN = 1.0e-9f;

    float dist = hypotf( input.x, input.y );
    if (dist < epsilonN)    // avoid divide by zero
        {
        dist = epsilonN;
        NOTE( "very small vector normalized\n" );
        }
    
    point2D result( input.x/dist, input.y/dist );
    return result;
}

inline float dotProduct( const point2D &input1, const point2D &input2 )
{
    float result = (input1.x * input2.x) + (input1.y * input2.y);
    return result;
}

/*********************************************************************/

inline point3D offsetPoint( const point3D &input, const point3D &offset )
{
    point3D result( input.x + offset.x, input.y + offset.y, input.z + offset.z );
    return result;
}

inline point3D scalePoint( const point3D &input, const float scale )
{
    point3D result( input.x * scale, input.y * scale, input.z * scale );
    return result;
}

inline point3D subtractPoints( const point3D &input1, const point3D &input2 )
{
    point3D result( input1.x - input2.x, input1.y - input2.y, input1.z - input2.z );
    return result;
}

inline point3D normalizeVector( const point3D &input )
{
    const float epsilonN = 1.0e-9f;

    float dist = sqrtf( input.x*input.x + input.y*input.y + input.z*input.z );
    if (dist < epsilonN)    // avoid divide by zero
        {
        dist = epsilonN;
        NOTE( "very small vector normalized\n" );
        }
    
    point3D result( input.x/dist, input.y/dist, input.z/dist );
    return result;
}

inline float dotProduct( const point3D &input1, const point3D &input2 )
{
    float result = (input1.x * input2.x) + (input1.y * input2.y) + (input1.z * input2.z);
    return result;
}

/*********************************************************************/

polygon rotatePolygon( const polygon &input, const float angle );

polygon createPolygonVectors( const polygon &input );

floatList createPolygonLengths( const polygon &input );

polygon offsetPolygon( const polygon &input, const point2D &offset );

void boundsPolygon( const polygon &input, point2D &min, point2D &max );

polygon reversePolygon( const polygon &input );

point3D triangleNormal( const point3D &p1, const point3D &p2, const point3D &p3 );

/*********************************************************************/

inline polygon centerPolygon( const polygon &input )
{
    point2D min, max;
    boundsPolygon( input, min, max );
    point2D offset( (min.x + max.x) / -2.0f, (min.y + max.y) / -2.0f );
    polygon result = offsetPolygon( input, offset );
    return result;
}

/*********************************************************************/

inline point2D findPolygonCenter( const polygon &input )
{
    point2D min, max;
    boundsPolygon( input, min, max );
    point2D center( (max.x+min.x)/2.0f, (max.y+min.y)/2.0f );
    return center;
}

/*********************************************************************/

polygon scalePolygon( const polygon &input, const float scale );

polygon scaleToFitPolygon( const polygon &input, const float maxValue );

float findMaxRadius( const polygon &input );

/*********************************************************************/

triangle_list scale3D( triangle_list triangles, float scale );
triangle_list offset3D( triangle_list triangles, point3D offset );

point3D Jitter3D( const float jitter );

/*********************************************************************/
/*********************************************************************/

#endif // GEOMETRY_H
