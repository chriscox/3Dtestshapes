//
//  utilities.h
//
//  Copyright © 2016-2023 Chris Cox. All rights reserved.
//  Distributed under the MIT License
//

#ifndef UTILITIES_H
#define UTILITIES_H

#include <vector>
#include <unordered_map>
#include "pooled_hashmap.h"
#include "utilities.hh"
#include "geometry.hh"

/*********************************************************************/

FILE *StartSVGFile( const char *base_testname );
void FinishSVGFile( FILE *out );

void AppendPolygonToSVGFile( FILE *out, const polygon &input );
void AppendPointToSVGFile( FILE *out, float x, float y );
void AppendCircleToSVGFile( FILE *out, float x, float y, float r );

/*********************************************************************/

size_t hashPoint3D( const point3D &p );
size_t hashPoint3DRounded( const point3D &p );

template <>
struct std::hash<point3D>
{
    size_t operator()(point3D __v) const _NOEXCEPT {return hashPoint3DRounded(__v);}
};

/*********************************************************************/

typedef std::vector<point3D> vertex_list;

typedef std::vector<index3> face_list;

//typedef std::unordered_map<point3D,size_t> vertexMap;
typedef PooledHashMap<point3D,size_t> vertexMap;

/*********************************************************************/

typedef class MULTI_3D_FILE {

public:
    MULTI_3D_FILE() {
        STLFile = NULL;
        STLBinaryFile = NULL;
        X3DFile = NULL;
        X3DIndexFile = NULL;
        PLYFile = NULL;
        PLYBinaryFile = NULL;
        AMFFile = NULL;
        OBJFile = NULL;
        DAEFile = NULL;
        VRMLFile = NULL;
        faces_hint = 0;
    }
    
    ~MULTI_3D_FILE();

public:
    FILE *STLFile;
    FILE *STLBinaryFile;
    FILE *X3DFile;
    FILE *X3DIndexFile;
    FILE *PLYFile;
    FILE *PLYBinaryFile;
    FILE *AMFFile;
    FILE *OBJFile;
    FILE *DAEFile;
    FILE *VRMLFile;

public:
    // storage for our geometry
    vertex_list vertices;
    face_list    faces;
    
    vertexMap    vertexLookup;
    size_t faces_hint;

} MULTI_3D_FILE;

/*********************************************************************/

MULTI_3D_FILE Start3DFile( const char *base_testname, const size_t faces_hint = 0 );
void Finish3DFile( MULTI_3D_FILE &out );

void Write3DFlatRectangle( MULTI_3D_FILE &out, const polygon &object, const float Z, const bool top );
void Write3DFlatPolygon( MULTI_3D_FILE &out, const polygon &object, const float Z, const bool top );
void Write3DConnectingEdges( MULTI_3D_FILE &out, const polygon &previous_poly, const polygon &current_poly, const float bottomZ, const float topZ );
void Write3DConnectingOuterEdges( MULTI_3D_FILE &out, const polygon &previous_poly, const polygon &current_poly, const float bottomZ, const float topZ, const float thickness );

void Write3DTessellatedRectangle( MULTI_3D_FILE &out, const size_t divisions,
                                    const point3D &base, const point3D &arm1, const point3D &arm2 );

void SaveTriangle( MULTI_3D_FILE &out, const point3D &p1, const point3D &p2, const point3D &p3 );

/*********************************************************************/
/*********************************************************************/

#endif    // UTILITIES_H
