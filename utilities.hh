//
//  utilities.hh
//
//  Copyright © 2016-2022 Chris Cox. All rights reserved.
//  Distributed under the MIT License
//

#ifndef UTILITIES_HH
#define UTILITIES_HH

#include <vector>
#include "config.h"

/*********************************************************************/

// included here so headers can use these error functions

// just print a note to console
void NOTE( const char *message );

// if condition is false, print a note to the console
void ASSERT( const bool condition, const char *message );

// if the condition is false, print a note to the console, then exit the process with an error
void REQUIRE( const bool condition, const char *message );

/*********************************************************************/

#endif    // UTILITIES_HH
