//
//  config.cpp
//
//  Copyright © 2017-2023 Chris Cox. All rights reserved.
//  Distributed under the MIT License
//

#include <cstdio>
#include <cstdlib>
#include <string.h>
#include <cmath>
#include "config.h"

/*********************************************************************/
/*********************************************************************/

// twisted extrusion settings

// how many steps in our rotation
size_t angle_count = 600;                // 600 / 60mm = 0.1mm resolution

// what is the final rotation, in degrees
float final_angle = 720;

// height of the object, in mm
float full_height = 60.0;

// diameter of the object, in mm
float diameter = 30.0;


// global configuration

bool outputSVGGeometry = false;
bool outputX3DFile = false;
bool outputX3DIndexFile = false;
bool outputSTLFile = false;
bool outputSTLBinaryFile = true;
bool outputPLYFile = false;
bool outputPLYBinaryFile = false;
bool outputAMFFile = false;
bool outputOBJFile = true;
bool outputDAEFile = false;
bool outputVRMLFile = false;


bool generateSmallFiles = true;
bool generateMediumFiles = true;
bool generateLargeFiles = false;
bool generateHugeFiles = false;

bool generateBenchmark = false;

/*********************************************************************/
/*********************************************************************/

void print_usage(const char *argv[])
{
    printf("Usage: %s <args>\n", argv[0] );
    printf("\t-small X            Generate smaller files over 1 Gig (default %s)\n", generateSmallFiles ? "1" : "0" );
    printf("\t-med(ium) X         Generate files less than 1 Gig (default %s)\n", generateMediumFiles ? "1" : "0" );
    printf("\t-large X            Generate files over 1 Gig (default %s)\n", generateLargeFiles ? "1" : "0" );
    printf("\t-huge X             Generate files over 10 Gig (default %s)\n", generateHugeFiles ? "1" : "0" );
    printf("\t-steps S            Number of steps for rotations (default %lu)\n", angle_count );
    printf("\t-final F            Final rotation angle in degrees (default %f)\n", final_angle );
    printf("\t-height H           Height of rotated object in mm (default %f)\n", full_height );
    printf("\t-dia(meter) D       Diameter of rotated object in mm (default %f)\n", diameter );
    printf("\t-stl X              Output ascii STL files (default %s)\n", outputSTLFile ? "1" : "0" );
    printf("\t-stlb X             Output binary STL files (default %s)\n", outputSTLBinaryFile ? "1" : "0" );
    printf("\t-obj X              Output OBJ files (default %s)\n", outputOBJFile ? "1" : "0" );
    printf("\t-x3d X              Output X3D files (default %s)\n", outputX3DFile ? "1" : "0" );
    printf("\t-x3di X             Output X3D indexed files (default %s)\n", outputX3DIndexFile ? "1" : "0" );
    printf("\t-ply X              Output ascii PLY files (default %s)\n", outputPLYFile ? "1" : "0" );
    printf("\t-plyb X             Output binary PLY files (default %s)\n", outputPLYBinaryFile ? "1" : "0" );
    printf("\t-amf X              Output AMF files (default %s)\n", outputAMFFile ? "1" : "0" );
    printf("\t-dae X              Output DAE files (default %s)\n", outputDAEFile ? "1" : "0" );
    printf("\t-vrml X             Output VRML files (default %s)\n", outputVRMLFile ? "1" : "0" );
    printf("\t-svg X              Output svg files for each geometry and rotation (default %s)\n", outputSVGGeometry ? "1" : "0" );
    printf("\t-bench(mark) X      Time creation of a subset of files (default %s)\n", generateBenchmark ? "1" : "0" );
    printf("Version %s, Compiled %s %s\n", kVersionString, __DATE__, __TIME__ );
}

/******************************************************************************/

void parse_arguments( int argc, const char *argv[] )
{
    const int min_angles = 1;
    const float min_height = 1.0;
    const float min_diameter = 1.0;


    // This should run with zero arguments

    for ( int c = 1; c < argc; ++c )
        {
        
        if ( (strcmp( argv[c], "-steps" ) == 0
            || strcmp( argv[c], "-Steps" ) == 0)
            && c < (argc-1) )
            {
            angle_count = (size_t) atoll( argv[c+1] );
            if (angle_count < min_angles)
                {
                printf("INFO: Invalid step count (%lu), pinning to %d\n", angle_count, min_angles);
                angle_count = min_angles;
                }
            ++c;
            }
        else if ( (strcmp( argv[c], "-final" ) == 0
                || strcmp( argv[c], "-Final" ) == 0)
                && c < (argc-1) )
            {
            final_angle = (float) atof( argv[c+1] );
            ++c;
            }
        else if ( (strcmp( argv[c], "-height" ) == 0
                || strcmp( argv[c], "-Height" ) == 0)
                && c < (argc-1) )
            {
            full_height = (float) atof( argv[c+1] );
            if (full_height < min_height)
                {
                printf("INFO: Invalid height (%f), pinning to %f\n", full_height, min_height);
                full_height = min_height;
                }
            ++c;
            }
        else if ( (strcmp( argv[c], "-dia" ) == 0
                || strcmp( argv[c], "-Dia" ) == 0
                || strcmp( argv[c], "-diameter" ) == 0
                || strcmp( argv[c], "-Diameter" ) == 0 )
                && c < (argc-1) )
            {
            diameter = (float) atof( argv[c+1] );
            if (diameter < min_height)
                {
                printf("INFO: Invalid diameter (%f), pinning to %f\n", diameter, min_diameter);
                diameter = min_diameter;
                }
            ++c;
            }
        else if ( (strcmp( argv[c], "-stl" ) == 0 || strcmp( argv[c], "-STL" ) == 0 )
            && c < (argc-1)  )
            {
            bool temp = atoi( argv[c+1] ) != 0;
            outputSTLFile = temp;
            ++c;
            }
        else if ( (strcmp( argv[c], "-stlb" ) == 0 || strcmp( argv[c], "-STLB" ) == 0 )
            && c < (argc-1)  )
            {
            bool temp = atoi( argv[c+1] ) != 0;
            outputSTLBinaryFile = temp;
            ++c;
            }
        else if ( (strcmp( argv[c], "-svg" ) == 0 || strcmp( argv[c], "-SVG" ) == 0 )
            && c < (argc-1)  )
            {
            bool temp = atoi( argv[c+1] ) != 0;
            outputSVGGeometry = temp;
            ++c;
            }
        else if ( (strcmp( argv[c], "-x3d" ) == 0 || strcmp( argv[c], "-X3D" ) == 0 )
            && c < (argc-1)  )
            {
            bool temp = atoi( argv[c+1] ) != 0;
            outputX3DFile = temp;
            ++c;
            }
        else if ( (strcmp( argv[c], "-x3di" ) == 0 || strcmp( argv[c], "-X3DI" ) == 0 )
            && c < (argc-1)  )
            {
            bool temp = atoi( argv[c+1] ) != 0;
            outputX3DIndexFile = temp;
            ++c;
            }
        else if ( (strcmp( argv[c], "-ply" ) == 0 || strcmp( argv[c], "-PLY" ) == 0 )
            && c < (argc-1)  )
            {
            bool temp = atoi( argv[c+1] ) != 0;
            outputPLYFile = temp;
            ++c;
            }
        else if ( (strcmp( argv[c], "-amf" ) == 0 || strcmp( argv[c], "-AMF" ) == 0 )
            && c < (argc-1)  )
            {
            bool temp = atoi( argv[c+1] ) != 0;
            outputAMFFile = temp;
            ++c;
            }
        else if ( (strcmp( argv[c], "-obj" ) == 0 || strcmp( argv[c], "-OBJ" ) == 0 )
            && c < (argc-1)  )
            {
            bool temp = atoi( argv[c+1] ) != 0;
            outputOBJFile = temp;
            ++c;
            }
        else if ( (strcmp( argv[c], "-dae" ) == 0 || strcmp( argv[c], "-DAE" ) == 0 )
            && c < (argc-1)  )
            {
            bool temp = atoi( argv[c+1] ) != 0;
            outputDAEFile = temp;
            ++c;
            }
        else if ( (strcmp( argv[c], "-vrml" ) == 0 || strcmp( argv[c], "-VRML" ) == 0 )
            && c < (argc-1)  )
            {
            bool temp = atoi( argv[c+1] ) != 0;
            outputVRMLFile = temp;
            ++c;
            }
        else if ( (strcmp( argv[c], "-small" ) == 0 || strcmp( argv[c], "-SMALL" ) == 0 )
            && c < (argc-1)  )
            {
            bool temp = atoi( argv[c+1] ) != 0;
            generateSmallFiles = temp;
            ++c;
            }
        else if ( (strcmp( argv[c], "-med" ) == 0 || strcmp( argv[c], "-MED" ) == 0
                   || strcmp( argv[c], "-medium" ) == 0 || strcmp( argv[c], "-MEDIUM" ) == 0 )
            && c < (argc-1)  )
            {
            bool temp = atoi( argv[c+1] ) != 0;
            generateMediumFiles = temp;
            ++c;
            }
        else if ( (strcmp( argv[c], "-large" ) == 0 || strcmp( argv[c], "-LARGE" ) == 0 )
            && c < (argc-1)  )
            {
            bool temp = atoi( argv[c+1] ) != 0;
            generateLargeFiles = temp;
            ++c;
            }
        else if ( (strcmp( argv[c], "-huge" ) == 0 || strcmp( argv[c], "-HUGE" ) == 0 )
            && c < (argc-1)  )
            {
            bool temp = atoi( argv[c+1] ) != 0;
            generateHugeFiles = temp;
            ++c;
            }
        else if ( (strcmp( argv[c], "-bench" ) == 0 || strcmp( argv[c], "-BENCH" ) == 0
                   || strcmp( argv[c], "-benchmark" ) == 0 || strcmp( argv[c], "-BENCHMARK" ) == 0 )
            && c < (argc-1)  )
            {
            bool temp = atoi( argv[c+1] ) != 0;
            generateBenchmark = temp;
            ++c;
            }
        else if ( strcmp( argv[c], "-V" ) == 0
                || strcmp( argv[c], "-v" ) == 0
                || strcmp( argv[c], "-help" ) == 0
                || strcmp( argv[c], "--help" ) == 0
                || strcmp( argv[c], "-version" ) == 0
                || strcmp( argv[c], "-Version" ) == 0
                )
            {
            print_usage( argv );
            exit (0);
            }
        else if (argv[c][0] == '-')
            {
            // unrecognized switch
            printf("ERROR: unrecognized command line option %s\n", argv[c] );
            print_usage( argv );
            exit (1);
            }
        
        }

}

/*********************************************************************/
/*********************************************************************/
