//
//  utilities.cpp
//
//  Copyright © 2016-2023 Chris Cox. All rights reserved.
//  Distributed under the MIT License
//


#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <climits>
#include <float.h>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include <functional>
#include "config.h"
#include "utilities.h"
#include "geometry.h"

/*********************************************************************/

void NOTE( const char *message )
{
    fprintf(stderr,"INFO - %s, please debug\n", message );
}

void ASSERT( const bool condition, const char *message )
{
    if (!condition)
        fprintf(stderr,"WARNING - %s, please debug\n", message );
}

void REQUIRE( const bool condition, const char *message )
{
    if (!condition)
        {
        fprintf(stderr,"FATAL - %s, please debug\n", message );
        exit(-1);
        }
}

/*********************************************************************/

bool endianLittleHateProgrammers()
{
    const uint32_t testLong = 0x01020304;
    const unsigned char *testChar = (unsigned char *)&testLong;
    bool littleEndian = (testChar[0] == 0x04);
    ASSERT( ((testChar[0] == 0x01) || (testChar[0] == 4)), "unexpected byte order");
    return littleEndian;
}

bool isSystemLittleEndian = endianLittleHateProgrammers();

/*********************************************************************/

// AB -> BA
unsigned short swabShort( const unsigned short x )
{
    unsigned short result = (unsigned short)((x >> 8) | ((x & 0x00FF) << 8));
    return result;
}

// ABCD -> DCBA
uint32_t swabLong( const uint32_t x )
{
    uint32_t result = (x >> 24) | ((x & 0x000000FF) << 24) | ((x >> 8) & 0x0000FF00) | ((x << 8) & 0x00FF0000);
    return result;
}

void SwabLongBuffer( uint32_t *buf, uint32_t count )
{
    for (uint32_t i=0; i<count; ++i)
        {
        uint32_t value = buf[i];
        buf[i] = swabLong(value);
        }
}

/*********************************************************************/
/*********************************************************************/

/*

fprintf is SLOW on formatting and writing
fwrite hits a lock on the way in and out - so additional buffering helps!  (@#%@#% libc/OS code)


// fprintf
USE_FAST_STRING_CONVERSION      0
% time ./testshapes
24.803u 2.849s 0:27.96 98.8%    0+0k 37+40io 0pf+0w
25.064u 2.970s 0:28.30 99.0%    0+0k 36+75io 0pf+0w
25.098u 3.381s 0:28.73 99.0%    0+0k 7+84io 0pf+0w

USE_FAST_STRING_CONVERSION      1
% time ./testshapes
16.218u 2.890s 0:21.35 89.4%    0+0k 18+61io 0pf+0w
16.195u 4.794s 0:27.78 75.5%    0+0k 32+59io 48285pf+0w
16.338u 2.871s 0:19.56 98.1%    0+0k 3+49io 0pf+0w

with reserve for large storage
12.662u 2.976s 0:15.82 98.7%    0+0k 12+51io 0pf+0w
12.771u 2.826s 0:16.02 97.3%    0+0k 560+29io 1pf+0w
12.703u 3.007s 0:15.92 98.6%    0+0k 33+40io 1pf+0w

with 8k buffer for writes
10.721u 2.676s 0:14.31 93.5%    0+0k 43+54io 0pf+0w
10.874u 2.863s 0:14.34 95.7%    0+0k 607+83io 0pf+0w
10.735u 4.129s 0:18.44 80.5%    0+0k 29+70io 25991pf+0w

with PooledHashMap, 32k buffered writes
8.383u 2.614s 0:25.17 43.6%	0+0k 0+58io 0pf+0w
8.400u 2.345s 0:14.52 73.9%	0+0k 0+34io 0pf+0w
8.458u 2.453s 0:16.40 66.4%	0+0k 0+59io 0pf+0w

current with stpcpy2
7.885u 2.271s 0:13.01 78.0%	0+0k 0+29io 0pf+0w
7.870u 2.417s 0:16.03 64.1%	0+0k 0+40io 0pf+0w
7.914u 2.475s 0:13.59 76.3%	0+0k 0+30io 0pf+0w


*/


#define USE_FAST_STRING_CONVERSION      1

#if USE_FAST_STRING_CONVERSION

/*
    For better ideas, see
    https://www.facebook.com/notes/10158791579037200/
    https://tia.mat.br/posts/2014/06/23/integer_to_string_conversion.html
    https://www.zverovich.net/2020/06/13/fast-int-to-string-revisited.html
    https://github.com/fmtlib/fmt
    
    Already the time spent in string conversion has gone down to 0.8s out of 10s.
        majority of time in write(), hash->find(), malloc/free (for hash entries)
 */

size_t toString(char *output, uint64_t x)
{
    const size_t max_string = 32;       // ULLONG_MAX == 18446744073709551615ULL, 20 digits
    char buffer[ max_string ];
    size_t pos = max_string - 1;
    
    while (x >= 10) {
        // compiler should use reciprocal and share code between divide and modulo
        uint32_t d = static_cast<uint32_t>(x % 10); // [0..9]
        x = x / 10;
        buffer[pos--] = (char)('0' + d);
    }
    buffer[pos] = (char)('0' + x);
    
    // copy result to output
    size_t len = max_string - pos;
    for (size_t i = 0; i < len; ++i)
        output[i] = buffer[pos+i];
    
    return len;
}

/*********************************************************************/

size_t toString(char *output, int64_t y)
{
    const size_t max_string = 32;       // LLONG_MAX  9223372036854775807LL, 19 digits, plus sign
    char buffer[ max_string ];
    size_t pos = max_string - 1;
    bool isNegative = false;
    
    if (y < 0) {
        y = -y;
        isNegative = true;
    }
    
    uint64_t x = (uint64_t) y; // now positive or zero
    
    while (x >= 10) {
        // compiler should use reciprocal and share code between divide and modulo
        uint32_t d = static_cast<uint32_t>(x % 10); // [0..9]
        x = x / 10;
        buffer[pos--] = (char)('0' + d);
    }
    buffer[pos] = (char)('0' + x);
    
    if (isNegative)
        buffer[--pos] = '-';
    
    // copy result to output
    size_t len = max_string - pos;
    for (size_t i = 0; i < len; ++i)
        output[i] = buffer[pos+i];
    
    return len;
}

/*********************************************************************/

/*
FLT_MAX = 3.40282e+38
FLT_MIN = 1.17549e-38
FLT_EPSILON = 1.19209e-07

DBL_MAX = 1.79769e+308
DBL_MIN = 2.22507e-308
DBL_EPSILON = 2.22045e-16

TODO - This will fail if the value before the decimal is larger than 2^64
        but float max is around 2^128 !
        should test for overflow, use extra values grouped by max digits (need to adjust each to hold right values)

TODO - this needs a lot of work before it can work with double precision

*/

// TODO - would it be faster to do this all forward?  (reverse/copy in pieces as needed)
size_t toString(char *output, float x) {
    const size_t frac_digits = 6;
    const size_t precision = 1000000;   // 10^frac_digits
    const float precisionf = precision;
    
    const size_t max_string = 64;       // -0. + 38 + 6 == 47
    char buffer[ max_string ];
    size_t pos = max_string - 1;
    bool isNegative = false;
    
    if (isnan(x)) {       // may not always work in MSVC because of compiler bugs
        output[0] = 'N';
        output[1] = 'a';
        output[2] = 'N';
        return 3;
    }
    
    if (x < 0.0) {
        x = -x;
        isNegative = true;
    }
    
    if (isinf(x)) {
        if (isNegative) {
            output[0] = '-';
            output[1] = 'I';
            output[2] = 'N';
            output[3] = 'F';
            return 4;
        } else {
            output[0] = 'I';
            output[1] = 'N';
            output[2] = 'F';
            return 3;
        }
    }
    
    // convert our float into two ints
    float temp = floorf(x);
    uint64_t first = static_cast<uint64_t>(temp);
    float fraction = x - temp;
    uint64_t second = static_cast<uint64_t>(precisionf * fraction + 0.5f);

    // fill any leading zeros
    for (size_t i = 0; i < frac_digits; ++i)
        buffer[pos-i] = '0';
    
    uint64_t y = second;
    if (y != 0) {
        while (y >= 10) {
            // compiler should use reciprocal and share code between divide and modulo
            uint32_t d = static_cast<uint32_t>(y % 10); // [0..9]
            y = y / 10;
            buffer[pos--] = (char)('0' + d);
        }
        //assert(y != 0);
        buffer[pos] = (char)('0' + y);
    }
    
    // this makes the first and second independent loops, which could be optimized
    pos = max_string - 1 - frac_digits;
    
    buffer[pos--] = '.';
    
    uint64_t z = first;
    while (z >= 10) {
        // compiler should use reciprocal and share code between divide and modulo
        uint32_t d = static_cast<uint32_t>(z % 10); // [0..9]
        z = z / 10;
        buffer[pos--] = (char)('0' + d);
    }
    buffer[pos] = (char)('0' + z);
    
    if (isNegative)
        buffer[--pos] = '-';
    
    // copy result to output
    size_t len = max_string - pos;
    for (size_t i = 0; i < len; ++i)
        output[i] = buffer[pos+i];
    
    return len;
}

/*********************************************************************/

char *stpcpy2( char *output, const char *input )
{
    if (input && output) {
        while (*input != 0)
            *output++ = *input++;
    }
    return output;
}

/*********************************************************************/

size_t threeIntsToSpacedString( char *output, size_t out_len, const char *prefix, const char *suffix, uint64_t x, uint64_t y, uint64_t z )
{
    const size_t max_int_string = 22;   // int64_t limit, which is more than we'll need
    char *out_end = output + out_len;
    
    char *next = output;
    
    if (prefix != NULL)
        next = stpcpy2( output, prefix );
    
    // handle int conversions
    size_t len = toString(next,x);
    next += len;
    *next++ = ' ';
    
    ASSERT( (out_end - next) >  (ptrdiff_t)(3*max_int_string), "string buffer too small" );
    
    len = toString(next,y);
    next += len;
    *next++ = ' ';
    
    ASSERT( (out_end - next) > (ptrdiff_t)(2*max_int_string), "string buffer too small" );
    
    len = toString(next,z);
    next += len;
    
    ASSERT( (out_end - next) > (ptrdiff_t)(max_int_string), "string buffer too small" );

    if (suffix != NULL)
        next = stpcpy2( next, suffix );
    
    *next++ = 0;
    
    len =  (size_t)(next - output - 1);    // don't include the terminating NULL
    REQUIRE( (next < out_end), "string buffer too small" );
    
    return len;
}

/*********************************************************************/

size_t threeFloatsToSpacedString( char *output, size_t out_len, const char *prefix, const char *suffix, float x, float y, float z )
{
    const size_t max_float_string = 64;   // float string limit, which is more than we'll need
    char *out_end = output + out_len;
    
    char *next = output;
    
    if (prefix != NULL)
        next = stpcpy2( output, prefix );
    
    // handle int conversions
    size_t len = toString(next,x);
    next += len;
    *next++ = ' ';
    
    ASSERT( (out_end - next) > (ptrdiff_t)(3*max_float_string), "string buffer too small" );
    
    len = toString(next,y);
    next += len;
    *next++ = ' ';
    
    ASSERT( (out_end - next) > (ptrdiff_t)(2*max_float_string), "string buffer too small" );
    
    len = toString(next,z);
    next += len;
    
    ASSERT( (out_end - next) > (ptrdiff_t)(max_float_string), "string buffer too small" );

    if (suffix != NULL)
        next = stpcpy2( next, suffix );
    
    *next++ = 0;
    
    len = (size_t)(next - output - 1);    // don't include the terminating NULL
    REQUIRE( (next < out_end), "string buffer too small" );
    
    return len;
}

#endif // USE_FAST_STRING_CONVERSION

/*********************************************************************/
/*********************************************************************/

const float SVGscale = 100.0f;
const float SVGWidth = 400.0f;
const float SVGOffset = SVGWidth / 2.0f;

/*********************************************************************/

static void WriteSVGHEader( FILE *out, int width, int height )
{
    fprintf(out, "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
    fprintf(out, "<svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"\n");
    fprintf(out, "\tx=\"0px\" y=\"0px\" viewBox=\"0 0 %d %d\" style=\"enable-background:new 0 0 %d %d;\" xml:space=\"preserve\">\n",
            width, height, width, height);
}

/*********************************************************************/

static void WriteSVGFooter( FILE *out )
{
    fprintf(out, "</svg>\n");
}

/*********************************************************************/

static void WriteSVGCircle( FILE *out, float x, float y, float r )
{
    x = x * SVGscale + SVGOffset;
    y = SVGWidth - (y * SVGscale + SVGOffset);
    r = r * SVGscale;
    fprintf(out, "<circle cx=\"%f\" cy=\"%f\" r=\"%f\" fill=\"red\" />\n",
        x, y, r );
}

/*********************************************************************/

static void WriteSVGLine( FILE *out, float x1, float y1, float x2, float y2 )
{
    x1 = x1 * SVGscale + SVGOffset;
    y1 = SVGWidth - (y1 * SVGscale + SVGOffset);
    x2 = x2 * SVGscale + SVGOffset;
    y2 = SVGWidth - (y2 * SVGscale + SVGOffset);
    fprintf(out, "<line x1=\"%f\" y1=\"%f\" x2=\"%f\" y2=\"%f\" stroke=\"green\" stroke-width=\"1\" />\n",
        x1, y1, x2, y2 );
}

/*********************************************************************/

void AppendPolygonToSVGFile( FILE *out, const polygon &input )
{
    if (!out)
        return;
    
    polygon::const_iterator iter = input.begin();
    float xx = iter->x * SVGscale + SVGOffset;
    float yy = SVGWidth - (iter->y * SVGscale + SVGOffset);
    fprintf(out,"<path d=\"M%f,%f", xx, yy );
    ++iter;
    
    for (; iter != input.end(); ++iter)
        {
        xx = iter->x * SVGscale + SVGOffset;
        yy = SVGWidth -(iter->y * SVGscale + SVGOffset);
        fprintf(out,"L%f,%f", xx, yy );
        }
    
    fprintf(out,"z\"/>\n");
}

/*********************************************************************/

void AppendPointToSVGFile( FILE *out, float x, float y )
{
    if (!out)
        return;

    WriteSVGCircle(out, x, y, 0.02f );
}

/*********************************************************************/

void AppendCircleToSVGFile( FILE *out, float x, float y, float r )
{
    if (!out)
        return;

    WriteSVGCircle(out, x, y, r );
}

/*********************************************************************/

void AppendCameraToSVGFile( FILE *out, point2D cam_center, point2D cam_vector, float angle_limit )
{
    if (!out)
        return;

    WriteSVGCircle(out, cam_center.x, cam_center.y, 0.1f );
    
    point2D cameraVectorL = rotatePoint( cam_vector, -angle_limit );
    point2D cameraVectorR = rotatePoint( cam_vector, angle_limit );
    
    WriteSVGLine(out, cam_center.x, cam_center.y, cam_center.x + cam_vector.x, cam_center.y + cam_vector.y );
    WriteSVGLine(out, cam_center.x, cam_center.y, cam_center.x + cameraVectorL.x, cam_center.y + cameraVectorL.y );
    WriteSVGLine(out, cam_center.x, cam_center.y, cam_center.x + cameraVectorR.x, cam_center.y + cameraVectorR.y );
}

/*********************************************************************/

FILE *StartSVGFile( const char *base_testname )
{
    if (!outputSVGGeometry)
        return NULL;

    const size_t string_limit = 1024;
    char filename[string_limit];
    
    if ( strlen(base_testname) > (string_limit-5) )
        {
        fprintf(stderr,"filename %s is too long for internal string limits\n", base_testname );
        return NULL;
        }

    snprintf( filename, string_limit, "%s.svg", base_testname );

    FILE *result = fopen( filename, "w" );
    
    if (!result)
        {
        fprintf(stderr,"WARNING - could not create file %s\n", filename );
        return NULL;
        }
    
    WriteSVGHEader( result, (int)SVGWidth, (int)SVGWidth );
    
    return result;
}

/*********************************************************************/

void FinishSVGFile( FILE *out )
{
    if (!out)
        return;
    
    WriteSVGFooter( out );
    fclose(out);
}

/*********************************************************************/
/*********************************************************************/

static void WriteSTLHeader( FILE *out )
{
    if (!out)
        return;
    fprintf(out, "solid model\n");
}

/*********************************************************************/

static void WriteSTLFooter( MULTI_3D_FILE &out )
{
    if (!out.STLFile)
        return;


#if USE_FAST_STRING_CONVERSION

    const size_t buffer_len = 32768;
    const size_t string_len = 256;
    char buffer[ buffer_len + 4*string_len];
    char *bufptr = buffer;
    char *bufend = buffer + buffer_len;
    
    face_list::const_iterator face = out.faces.begin();

    for (; face != out.faces.end(); ++face)
        {
        point3D p1 = out.vertices[face->x];
        point3D p2 = out.vertices[face->y];
        point3D p3 = out.vertices[face->z];
    
        point3D normal = triangleNormal( p1, p2, p3 );
    
        size_t len = threeFloatsToSpacedString(bufptr, string_len, "facet normal ", "\n", normal.x, normal.y, normal.z );
        bufptr += len;
    
        len = threeFloatsToSpacedString(bufptr, string_len, "outer loop\nvertex ", "\n", p1.x, p1.y, p1.z );
        bufptr += len;
        
        len = threeFloatsToSpacedString(bufptr, string_len, "vertex ", "\n", p2.x, p2.y, p2.z );
        bufptr += len;
    
        len = threeFloatsToSpacedString(bufptr, string_len, "vertex ", "\nendloop\nendfacet\n", p3.x, p3.y, p3.z );
        bufptr += len;
        
        if (bufptr >= bufend)
            {
            fwrite( buffer, (size_t)(bufptr-buffer), 1, out.STLFile );
            bufptr = buffer;
            }
        }

    if (bufptr != buffer)
        fwrite( buffer, (size_t)(bufptr-buffer), 1, out.STLFile );

#else
    face_list::const_iterator face = out.faces.begin();
    
    for (; face != out.faces.end(); ++face)
        {
        point3D p1 = out.vertices[face->x];
        point3D p2 = out.vertices[face->y];
        point3D p3 = out.vertices[face->z];
    
        point3D normal = triangleNormal( p1, p2, p3 );

        fprintf(out.STLFile, "facet normal %f %f %f\n", normal.x, normal.y, normal.z );
        fprintf(out.STLFile, "outer loop\n");
        fprintf(out.STLFile, "vertex %f %f %f\n", p1.x, p1.y, p1.z );
        fprintf(out.STLFile, "vertex %f %f %f\n", p2.x, p2.y, p2.z );
        fprintf(out.STLFile, "vertex %f %f %f\n", p3.x, p3.y, p3.z );
        fprintf(out.STLFile, "endloop\n");
        fprintf(out.STLFile, "endfacet\n");
        }
#endif
    
    fprintf(out.STLFile, "endsolid model\n");
}

/*********************************************************************/
/*********************************************************************/

static void WriteSTLBinaryHeader( FILE *out )
{
    if (!out)
        return;
}

/*********************************************************************/

static void WriteSTLBinaryFooter( MULTI_3D_FILE &out )
{
    if (!out.STLBinaryFile)
        return;

    uint32_t triCount = (uint32_t) out.faces.size();
    
    if (triCount != out.faces.size()) {
        fprintf(stderr,"WARNING - overflowed STL Binary face count limit\n");
        return;
    }
    
    unsigned char tempbuf[80];
    memset( tempbuf, ' ', 80 );    // 80 bytes of spaces
    fwrite( tempbuf, 80, 1, out.STLBinaryFile );
    
    if (!isSystemLittleEndian)
        triCount = swabLong(triCount);
    
    fwrite( &triCount, 4, 1, out.STLBinaryFile);    // 4 byte triangle count
    
/*
REAL32[3] – Normal vector
REAL32[3] – Vertex 1
REAL32[3] – Vertex 2
REAL32[3] – Vertex 3
UINT16 – Attribute byte count (zero or FFFF)

*/

#if USE_FAST_STRING_CONVERSION

    const size_t buffer_len = 32768;
    const size_t string_len = 256;
    uint8_t buffer[ buffer_len + string_len];
    uint8_t *bufptr = buffer;
    uint8_t *bufend = buffer + buffer_len;
    const size_t faceBytes = 4*12 + 2;

    face_list::const_iterator face = out.faces.begin();
    
    for (; face != out.faces.end(); ++face)
        {
        float triBuffer[ 13 ];        // 4*12 + 2 bytes
        
        point3D p1 = out.vertices[face->x];
        point3D p2 = out.vertices[face->y];
        point3D p3 = out.vertices[face->z];
    
        point3D normal = triangleNormal( p1, p2, p3 );
        
        triBuffer[0] = (float) normal.x;
        triBuffer[1] = (float) normal.y;
        triBuffer[2] = (float) normal.z;
        
        triBuffer[3] = (float) p1.x;
        triBuffer[4] = (float) p1.y;
        triBuffer[5] = (float) p1.z;
        
        triBuffer[6] = (float) p2.x;
        triBuffer[7] = (float) p2.y;
        triBuffer[8] = (float) p2.z;
        
        triBuffer[9]  = (float) p3.x;
        triBuffer[10] = (float) p3.y;
        triBuffer[11] = (float) p3.z;

        *(uint32_t *)(&triBuffer[12]) = 0xFFFFFFFF;
        
        // STL Binary is LittleEndian for some reason
        if (!isSystemLittleEndian)
            SwabLongBuffer((uint32_t *)triBuffer, 12);

        memcpy( bufptr, triBuffer, faceBytes );
        bufptr += faceBytes;
        
        if (bufptr >= bufend)
            {
            fwrite( buffer, (size_t)(bufptr-buffer), 1, out.STLBinaryFile );
            bufptr = buffer;
            }
    }

    if (bufptr != buffer)
        fwrite( buffer, (size_t)(bufptr-buffer), 1, out.STLBinaryFile );
    
#else
    face_list::const_iterator face = out.faces.begin();
    
    for (; face != out.faces.end(); ++face)
        {
        float triBuffer[ 13 ];        // 4*12 + 2 bytes
        
        point3D p1 = out.vertices[face->x];
        point3D p2 = out.vertices[face->y];
        point3D p3 = out.vertices[face->z];
    
        point3D normal = triangleNormal( p1, p2, p3 );
        
        triBuffer[0] = (float) normal.x;
        triBuffer[1] = (float) normal.y;
        triBuffer[2] = (float) normal.z;
        
        triBuffer[3] = (float) p1.x;
        triBuffer[4] = (float) p1.y;
        triBuffer[5] = (float) p1.z;
        
        triBuffer[6] = (float) p2.x;
        triBuffer[7] = (float) p2.y;
        triBuffer[8] = (float) p2.z;
        
        triBuffer[9]  = (float) p3.x;
        triBuffer[10] = (float) p3.y;
        triBuffer[11] = (float) p3.z;

        *(uint32_t *)(&triBuffer[12]) = 0xFFFFFFFF;
        
        // STL Binary is LittleEndian for some reason
        if (!isSystemLittleEndian)
            SwabLongBuffer((uint32_t *)triBuffer, 12);

        fwrite(triBuffer, 4*12+2, 1, out.STLBinaryFile);
    }
#endif
    
}

/*********************************************************************/
/*********************************************************************/

static void WriteX3DHeader( FILE *out)
{
    if (!out)
        return;
    fprintf(out, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
    fprintf(out, "<!DOCTYPE X3D PUBLIC \"ISO//Web3D//DTD X3D 3.2//EN\"\n");
    fprintf(out, "  \"http://www.web3d.org/specifications/x3d-3.2.dtd\">\n");
    fprintf(out, "<X3D profile=\"Interchange\" version=\"3.2\"\n");
    fprintf(out, "     xmlns:xsd=\"http://www.w3.org/2001/XMLSchema-instance\"\n");
    fprintf(out, "     xsd:noNamespaceSchemaLocation=\"http://www.web3d.org/specifications/x3d-3.2.xsd\">\n");
    fprintf(out, "<Scene>\n");
    fprintf(out, "  <Shape>\n");
    fprintf(out, "        <TriangleSet solid='true' ccw='true' colorPerVertex='false' normalPerVertex='false' containerField='geometry'>\n");
    fprintf(out, "        <Coordinate point=' ");
}

/*********************************************************************/

static void WriteX3DFooter( MULTI_3D_FILE &out )
{
    if (!out.X3DFile)
        return;
    
    face_list::const_iterator face = out.faces.begin();

#if USE_FAST_STRING_CONVERSION

    const size_t buffer_len = 32768;
    const size_t string_len = 256;
    char buffer[ buffer_len + 3*string_len];
    char *bufptr = buffer;
    char *bufend = buffer + buffer_len;

    for (; face != out.faces.end(); ++face)
        {
        point3D p1 = out.vertices[face->x];
        point3D p2 = out.vertices[face->y];
        point3D p3 = out.vertices[face->z];
    
        size_t len = threeFloatsToSpacedString(bufptr, string_len, " ", " ", p1.x, p1.y, p1.z );
        bufptr += len;
        
        len = threeFloatsToSpacedString(bufptr, string_len, " ", " ", p2.x, p2.y, p2.z );
        bufptr += len;
    
        len = threeFloatsToSpacedString(bufptr, string_len, " ", " ", p3.x, p3.y, p3.z );
        bufptr += len;
        
        if (bufptr >= bufend)
            {
            fwrite( buffer, (size_t)(bufptr-buffer), 1, out.X3DFile );
            bufptr = buffer;
            }
        
        }

    if (bufptr != buffer)
        fwrite( buffer, (size_t)(bufptr-buffer), 1, out.X3DFile );

#else
    for (; face != out.faces.end(); ++face)
        {
        point3D p1 = out.vertices[face->x];
        point3D p2 = out.vertices[face->y];
        point3D p3 = out.vertices[face->z];
        fprintf(out.X3DFile, " %f %f %f ", p1.x, p1.y, p1.z );
        fprintf(out.X3DFile, " %f %f %f ", p2.x, p2.y, p2.z );
        fprintf(out.X3DFile, " %f %f %f ", p3.x, p3.y, p3.z );
        }
#endif
    
    fprintf(out.X3DFile, " '/>\n");
    fprintf(out.X3DFile, "        </TriangleSet>\n");
    fprintf(out.X3DFile, "      </Shape>\n");
    fprintf(out.X3DFile, "</Scene>\n");
    fprintf(out.X3DFile, "</X3D>\n");
}

/*********************************************************************/
/*********************************************************************/

static void WriteX3DIndexHeader( FILE *out)
{
    if (!out)
        return;
    
    fprintf(out, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
    fprintf(out, "<!DOCTYPE X3D PUBLIC \"ISO//Web3D//DTD X3D 3.2//EN\"\n");
    fprintf(out, "  \"http://www.web3d.org/specifications/x3d-3.2.dtd\">\n");
    fprintf(out, "<X3D profile=\"Interchange\" version=\"3.2\"\n");
    fprintf(out, "     xmlns:xsd=\"http://www.w3.org/2001/XMLSchema-instance\"\n");
    fprintf(out, "     xsd:noNamespaceSchemaLocation=\"http://www.web3d.org/specifications/x3d-3.2.xsd\">\n");
    fprintf(out, "<Scene>\n");
    fprintf(out, "  <Shape>\n");

}

/*********************************************************************/

static void WriteX3DIndexFooter( MULTI_3D_FILE &out )
{
    if (!out.X3DIndexFile)
        return;

	fprintf(out.X3DIndexFile, " <IndexedFaceSet solid='true' ccw='true' colorPerVertex='false' ");

    fprintf(out.X3DIndexFile, "coordIndex='");

#if USE_FAST_STRING_CONVERSION

    const size_t buffer_len = 32768;
    const size_t string_len = 256;
    char buffer[ buffer_len + string_len];
    char *bufptr = buffer;
    char *bufend = buffer + buffer_len;

    face_list::const_iterator face = out.faces.begin();
    for (; face != out.faces.end(); ++face)
        {
        size_t len = threeIntsToSpacedString(bufptr, string_len, " ", " -1", face->x, face->y, face->z );
        bufptr += len;
        
        if (bufptr >= bufend)
            {
            fwrite( buffer, (size_t)(bufptr-buffer), 1, out.X3DIndexFile );
            bufptr = buffer;
            }
        }

    if (bufptr != buffer)
        fwrite( buffer, (size_t)(bufptr-buffer), 1, out.X3DIndexFile );

#else
	face_list::const_iterator face = out.faces.begin();
	for (; face != out.faces.end(); ++face)
		{
		fprintf(out.X3DIndexFile, " %llu %llu %llu -1", face->x, face->y, face->z );
		}
#endif

	fprintf(out.X3DIndexFile, "' ");
    
    fprintf(out.X3DIndexFile, "normalPerVertex='false' ");

    fprintf(out.X3DIndexFile, ">\n");

	fprintf(out.X3DIndexFile, " <Coordinate point='");
 
 
#if USE_FAST_STRING_CONVERSION
    bufptr = buffer;
    bufend = buffer + buffer_len;

    vertex_list::const_iterator iter = out.vertices.begin();
    
    for (; iter != out.vertices.end(); ++iter)
        {
        size_t len = threeFloatsToSpacedString(bufptr, string_len, " ", "", iter->x, iter->y, iter->z );
        bufptr += len;
        
        if (bufptr >= bufend)
            {
            fwrite( buffer, (size_t)(bufptr-buffer), 1, out.X3DIndexFile );
            bufptr = buffer;
            }
        }

    if (bufptr != buffer)
        fwrite( buffer, (size_t)(bufptr-buffer), 1, out.X3DIndexFile );

#else
	vertex_list::const_iterator vert = out.vertices.begin();
	for (; vert != out.vertices.end(); ++vert)
		{
		fprintf(out.X3DIndexFile, " %f %f %f", vert->x, vert->y, vert->z );
		}
#endif

	fprintf(out.X3DIndexFile, "'/>\n");
	
	fprintf(out.X3DIndexFile, " </IndexedFaceSet>\n");
	fprintf(out.X3DIndexFile, " </Shape>\n");
	fprintf(out.X3DIndexFile, "</Scene>\n");
	fprintf(out.X3DIndexFile, "</X3D>\n");
}

/*********************************************************************/
/*********************************************************************/

static void WriteOBJHeader( FILE *out )
{
}

/*********************************************************************/

static void WriteOBJFooter( MULTI_3D_FILE &out )
{
    if (!out.OBJFile)
        return;

#if USE_FAST_STRING_CONVERSION

    const size_t buffer_len = 32768;
    const size_t string_len = 256;
    char buffer[ buffer_len + string_len];
    char *bufptr = buffer;
    char *bufend = buffer + buffer_len;

    vertex_list::const_iterator iter = out.vertices.begin();
    
    for (; iter != out.vertices.end(); ++iter)
        {
        size_t len = threeFloatsToSpacedString(bufptr, string_len, "v ", "\n", iter->x, iter->y, iter->z );
        bufptr += len;
        
        if (bufptr >= bufend)
            {
            fwrite( buffer, (size_t)(bufptr-buffer), 1, out.OBJFile );
            bufptr = buffer;
            }
        }


    face_list::const_iterator face = out.faces.begin();
    
    for (; face != out.faces.end(); ++face)
        {
        size_t len = threeIntsToSpacedString(bufptr, string_len, "f ", "\n", 1+face->x, 1+face->y, 1+face->z );
        bufptr += len;
        
        if (bufptr >= bufend)
            {
            fwrite( buffer, (size_t)(bufptr-buffer), 1, out.OBJFile );
            bufptr = buffer;
            }
        }

    if (bufptr != buffer)
        fwrite( buffer, (size_t)(bufptr-buffer), 1, out.OBJFile );

#else

    vertex_list::const_iterator iter = out.vertices.begin();
    
    for (; iter != out.vertices.end(); ++iter)
        {
        fprintf(out.OBJFile, "v %f %f %f\n", iter->x, iter->y, iter->z);
        }


    face_list::const_iterator face = out.faces.begin();
    
    for (; face != out.faces.end(); ++face)
        {
        fprintf(out.OBJFile, "f %llu %llu %llu\n", 1+face->x, 1+face->y, 1+face->z );
        }

#endif

}

/*********************************************************************/
/*********************************************************************/

static void WritePLYHeader( FILE *out )
{
    if (!out)
        return;
    
    fprintf(out, "ply\n");
    fprintf(out, "format ascii 1.0\n");
}

/*********************************************************************/

static void WritePLYFooter( MULTI_3D_FILE &out )
{
    if (!out.PLYFile)
        return;
    
    int vertexCount = (int) out.vertices.size();
    int triCount = (int) out.faces.size();
    
    fprintf(out.PLYFile, "element vertex %d\n", vertexCount );
    fprintf(out.PLYFile, "property float x\n");
    fprintf(out.PLYFile, "property float y\n");
    fprintf(out.PLYFile, "property float z\n");
    fprintf(out.PLYFile, "element face %d\n",  triCount );
    fprintf(out.PLYFile, "property list uchar int vertex_index\n");
    fprintf(out.PLYFile, "end_header\n");


#if USE_FAST_STRING_CONVERSION

    const size_t buffer_len = 32768;
    const size_t string_len = 256;
    char buffer[ buffer_len + string_len ];
    char *bufptr = buffer;
    char *bufend = buffer + buffer_len;

    vertex_list::const_iterator iter = out.vertices.begin();
    
    for (; iter != out.vertices.end(); ++iter)
        {
        size_t len = threeFloatsToSpacedString(bufptr, string_len, "", "\n", iter->x, iter->y, iter->z );
        bufptr += len;
        
        if (bufptr >= bufend)
            {
            fwrite( buffer, (size_t)(bufptr-buffer), 1, out.PLYFile );
            bufptr = buffer;
            }
        }


    face_list::const_iterator face = out.faces.begin();
    
    for (; face != out.faces.end(); ++face)
        {
        size_t len = threeIntsToSpacedString(bufptr, string_len, "3 ", "\n", face->x, face->y, face->z );
        bufptr += len;
        
        if (bufptr >= bufend)
            {
            fwrite( buffer, (size_t)(bufptr-buffer), 1, out.PLYFile );
            bufptr = buffer;
            }
        }

    if (bufptr != buffer)
        fwrite( buffer, (size_t)(bufptr-buffer), 1, out.PLYFile );

#else
    vertex_list::const_iterator iter = out.vertices.begin();
    
    for (; iter != out.vertices.end(); ++iter)
        {
        fprintf(out.PLYFile, "%f %f %f\n", iter->x, iter->y, iter->z);
        }


    face_list::const_iterator face = out.faces.begin();
    
    for (; face != out.faces.end(); ++face)
        {
        fprintf(out.PLYFile, "3 %llu %llu %llu\n", face->x, face->y, face->z );
        }
#endif
}

/*********************************************************************/
/*********************************************************************/

static void WritePLYBinaryHeader( FILE *out )
{
    if (!out)
        return;
}

/*********************************************************************/

static void WritePLYBinaryFooter( MULTI_3D_FILE &out )
{
    if (!out.PLYBinaryFile)
        return;

    uint32_t vertCount = (uint32_t) out.vertices.size();
    
    if (vertCount != out.vertices.size()) {
        fprintf(stderr,"WARNING - overflowed PLY Binary vertex index limit\n");
        return;
    }
    
    // little endian seems more popular for PLY readers (some lame code fails to handle bigendian)
    fprintf(out.PLYBinaryFile, "ply\n");
    fprintf(out.PLYBinaryFile, "format binary_little_endian 1.0\n");
    
    int vertexCount = (int) out.vertices.size();
    int triCount = (int) out.faces.size();
    
    fprintf(out.PLYBinaryFile, "element vertex %d\n", vertexCount );
    fprintf(out.PLYBinaryFile, "property float x\n");
    fprintf(out.PLYBinaryFile, "property float y\n");
    fprintf(out.PLYBinaryFile, "property float z\n");
    
    fprintf(out.PLYBinaryFile, "element face %d\n",  triCount );
    fprintf(out.PLYBinaryFile, "property list uchar int vertex_index\n");
    fprintf(out.PLYBinaryFile, "end_header\n");

    float triBuffer[ 6 ];

    vertex_list::const_iterator iter = out.vertices.cbegin();

    for (; iter != out.vertices.end(); ++iter)
        {
        triBuffer[0] = iter->x;
        triBuffer[1] = iter->y;
        triBuffer[2] = iter->z;
        
        if (!isSystemLittleEndian)
            SwabLongBuffer((uint32_t *)triBuffer, 3);
        
        fwrite( triBuffer, 3*4, 1, out.PLYBinaryFile );
        }


    uint8_t faceBuffer[ 13 ];
    uint32_t *faceX = (uint32_t *)(faceBuffer + 1);
    uint32_t *faceY = (uint32_t *)(faceBuffer + 1 + 4);
    uint32_t *faceZ = (uint32_t *)(faceBuffer + 1 + 8);
    
    faceBuffer[0] = 3;
    
    face_list::const_iterator face = out.faces.begin();
    
    for (; face != out.faces.end(); ++face)
        {
        *faceX = uint32_t(face->x);
        *faceY = uint32_t(face->y);
        *faceZ = uint32_t(face->z);
        
        if (!isSystemLittleEndian)
            SwabLongBuffer((uint32_t *)(faceBuffer+1), 3);
        
        fwrite( faceBuffer, 1+3*4, 1, out.PLYBinaryFile );
        }
}

/*********************************************************************/
/*********************************************************************/

static void WriteAMFHeader( FILE *out )
{
    if (!out)
        return;

    fprintf(out, "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
    fprintf(out, "<amf unit=\"millimeter\" version=\"1.1\">\n");
    fprintf(out, "  <object id=\"1\">\n");
    fprintf(out, "    <mesh>\n");
}

/*********************************************************************/

static void WriteAMFFooter( MULTI_3D_FILE &out )
{
    if (!out.AMFFile)
        return;
    
    vertex_list::const_iterator iter = out.vertices.begin();

// TODO - use optimized string conversion and buffer - needs modified code for XML bogosity
    fprintf(out.AMFFile,"      <vertices>\n");
    for (; iter != out.vertices.end(); ++iter)
        {
        fprintf(out.AMFFile, "        <vertex><coordinates><x>%f</x><y>%f</y><z>%f</z></coordinates></vertex>\n", iter->x, iter->y, iter->z);
        }
    fprintf(out.AMFFile,"      </vertices>\n");


    face_list::const_iterator face = out.faces.begin();
    
// TODO - use optimized string conversion and buffer - needs modified code for XML bogosity
    fprintf(out.AMFFile,"      <volume>\n");
    for (; face != out.faces.end(); ++face)
        {
        fprintf(out.AMFFile, "        <triangle><v1>%llu</v1><v2>%llu</v2><v3>%llu</v3></triangle>\n", face->x, face->y, face->z );
        }
    fprintf(out.AMFFile,"      </volume>\n");

    fprintf(out.AMFFile,"     </mesh>\n");
    fprintf(out.AMFFile,"  </object>\n");
    fprintf(out.AMFFile,"</amf>\n");

}

/*********************************************************************/
/*********************************************************************/

static void WriteDAEHeader( FILE *out )
{
    if (!out)
        return;

    fprintf(out, "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
    fprintf(out, "<COLLADA xmlns=\"http://www.collada.org/2005/11/COLLADASchema\" version=\"1.4.1\">\n");
    fprintf(out, "<asset>\n");
    fprintf(out, "  <contributor>\n");
    fprintf(out, "    <authoring_tool>3DRotatedPolygon</authoring_tool>\n");
    fprintf(out, "  </contributor>\n");
    fprintf(out, "  <unit name=\"millimeters\" meter=\"0.001\"/>\n");
    fprintf(out, "  <up_axis>Z_UP</up_axis>\n");
    fprintf(out, "</asset>\n");

}

/*********************************************************************/

static void WriteDAEFooter( MULTI_3D_FILE &out )
{
    if (!out.DAEFile)
        return;
    
    int vertex_count = (int)out.vertices.size();
    int face_count = (int)out.faces.size();

    fprintf(out.DAEFile,"  <library_geometries>\n");
    fprintf(out.DAEFile,"  <geometry id=\"mesh1-geometry\" name=\"mesh1-geometry\">\n");
    fprintf(out.DAEFile,"    <mesh>\n");
    fprintf(out.DAEFile,"        <source id=\"mesh1-geometry-position\">\n");
    
    
    
    fprintf(out.DAEFile,"        <float_array id=\"mesh1-geometry-position-array\" count=\"%d\">", 3*vertex_count);

#if USE_FAST_STRING_CONVERSION

    const size_t buffer_len = 32768;
    const size_t string_len = 256;
    char buffer[ buffer_len + string_len ];
    char *bufptr = buffer;
    char *bufend = buffer + buffer_len;

    vertex_list::const_iterator iter = out.vertices.begin();
    
    for (; iter != out.vertices.end(); ++iter)
        {
        size_t len = threeFloatsToSpacedString(bufptr, string_len, "", " ", iter->x, iter->y, iter->z );
        bufptr += len;
        
        if (bufptr >= bufend)
            {
            fwrite( buffer, (size_t)(bufptr-buffer), 1, out.DAEFile );
            bufptr = buffer;
            }
        }

    if (bufptr != buffer)
        fwrite( buffer, (size_t)(bufptr-buffer), 1, out.DAEFile );

#else
    vertex_list::const_iterator iter = out.vertices.begin();
    for (; iter != out.vertices.end(); ++iter)
        {
        fprintf(out.DAEFile, "%f %f %f ", iter->x, iter->y, iter->z);
        }
#endif

    fprintf(out.DAEFile," </float_array>\n");
    
    fprintf(out.DAEFile,"          <technique_common>\n");
    fprintf(out.DAEFile,"            <accessor source=\"#mesh1-geometry-position-array\" count=\"%d\" stride=\"3\">\n", vertex_count);
    fprintf(out.DAEFile,"              <param name=\"X\" type=\"float\"/>\n");
    fprintf(out.DAEFile,"              <param name=\"Y\" type=\"float\"/>\n");
    fprintf(out.DAEFile,"              <param name=\"Z\" type=\"float\"/>\n");
    fprintf(out.DAEFile,"            </accessor>\n");
    fprintf(out.DAEFile,"          </technique_common>\n");
    fprintf(out.DAEFile,"        </source>\n");
    fprintf(out.DAEFile,"      <vertices id=\"mesh1-geometry-vertex\">\n");
    fprintf(out.DAEFile,"        <input semantic=\"POSITION\" source=\"#mesh1-geometry-position\"/>\n");
    fprintf(out.DAEFile,"      </vertices>\n");

    fprintf(out.DAEFile,"      <triangles count=\"%d\">\n", face_count );
    fprintf(out.DAEFile,"        <input semantic=\"VERTEX\" source=\"#mesh1-geometry-vertex\" offset=\"0\"/>\n");
    
    
    
    fprintf(out.DAEFile,"         <p> ");
    
#if USE_FAST_STRING_CONVERSION

    bufptr = buffer;
    bufend = buffer + buffer_len;

    face_list::const_iterator face = out.faces.begin();
    
    for (; face != out.faces.end(); ++face)
        {
        size_t len = threeIntsToSpacedString(bufptr, string_len, "", " ", face->x, face->y, face->z );
        bufptr += len;
        
        if (bufptr >= bufend)
            {
            fwrite( buffer, (size_t)(bufptr-buffer), 1, out.DAEFile );
            bufptr = buffer;
            }
        }

    if (bufptr != buffer)
        fwrite( buffer, (size_t)(bufptr-buffer), 1, out.DAEFile );

#else
    face_list::const_iterator face = out.faces.begin();
    for (; face != out.faces.end(); ++face)
        {
        fprintf(out.DAEFile, "%llu %llu %llu ", face->x, face->y, face->z );
        }
#endif
    fprintf(out.DAEFile," </p>\n");
    
    fprintf(out.DAEFile,"      </triangles>\n");
    fprintf(out.DAEFile,"    </mesh>\n");
    fprintf(out.DAEFile,"  </geometry>\n");
    fprintf(out.DAEFile,"</library_geometries>\n");

    fprintf(out.DAEFile,"<library_visual_scenes>\n");
    fprintf(out.DAEFile,"  <visual_scene id=\"Scene\" name=\"Scene\">\n");
    fprintf(out.DAEFile,"    <node id=\"Model\" name=\"Model\">\n");
    fprintf(out.DAEFile,"      <node id=\"mesh1\" name=\"mesh1\">\n");
    fprintf(out.DAEFile,"        <instance_geometry url=\"#mesh1-geometry\">\n");
    fprintf(out.DAEFile,"        </instance_geometry>\n");
    fprintf(out.DAEFile,"      </node>\n");
    fprintf(out.DAEFile,"    </node>\n");
    fprintf(out.DAEFile,"  </visual_scene>\n");
    fprintf(out.DAEFile,"</library_visual_scenes>\n");
    fprintf(out.DAEFile,"<scene>\n");
    fprintf(out.DAEFile,"  <instance_visual_scene url=\"#Scene\"/>\n");
    fprintf(out.DAEFile,"</scene>\n");
    fprintf(out.DAEFile,"</COLLADA>\n");

}

/*********************************************************************/
/*********************************************************************/

static void WriteVRMLHeader( FILE *out )
{
    if (!out)
        return;

}

/*********************************************************************/

static void WriteVRMLFooter( MULTI_3D_FILE &out )
{
    if (!out.VRMLFile)
        return;
    
	fprintf(out.VRMLFile, "#VRML V2.0 utf8\n");
	fprintf(out.VRMLFile, "Transform { children [ \n");
	fprintf(out.VRMLFile, "Shape {\n");
	
	fprintf(out.VRMLFile, "geometry IndexedFaceSet {\n");
	fprintf(out.VRMLFile, " coord DEF group_0_1Coords Coordinate { point [\n");
	
#if USE_FAST_STRING_CONVERSION

    const size_t buffer_len = 32768;
    const size_t string_len = 256;
    char buffer[ buffer_len + string_len ];
    char *bufptr = buffer;
    char *bufend = buffer + buffer_len;

    vertex_list::const_iterator iter = out.vertices.begin();
    
    for (; iter != out.vertices.end(); ++iter)
        {
        size_t len = threeFloatsToSpacedString(bufptr, string_len, "", ",\n", iter->x, iter->y, iter->z );
        bufptr += len;
        
        if (bufptr >= bufend)
            {
            fwrite( buffer, (size_t)(bufptr-buffer), 1, out.VRMLFile );
            bufptr = buffer;
            }
        }

    if (bufptr != buffer)
        fwrite( buffer, (size_t)(bufptr-buffer), 1, out.VRMLFile );

#else
	vertex_list::const_iterator iter = out.vertices.begin();
	for (; iter != out.vertices.end(); ++iter)
		{
		fprintf(out.VRMLFile, "%f %f %f,\n", iter->x, iter->y, iter->z);
		}
#endif

	fprintf(out.VRMLFile," ] }\n");

	fprintf(out.VRMLFile,"coordIndex [\n");

#if USE_FAST_STRING_CONVERSION
    bufptr = buffer;
    bufend = buffer + buffer_len;

    face_list::const_iterator face = out.faces.begin();
    
    for (; face != out.faces.end(); ++face)
        {
        size_t len = threeIntsToSpacedString(bufptr, string_len, "", " -1,\n", face->x, face->y, face->z );
        bufptr += len;
        
        if (bufptr >= bufend)
            {
            fwrite( buffer, (size_t)(bufptr-buffer), 1, out.VRMLFile );
            bufptr = buffer;
            }
        }

    if (bufptr != buffer)
        fwrite( buffer, (size_t)(bufptr-buffer), 1, out.VRMLFile );

#else
	face_list::const_iterator face = out.faces.begin();
	for (; face != out.faces.end(); ++face)
		{
		fprintf(out.VRMLFile, "%llu %llu %llu -1,\n", face->x, face->y, face->z );
		}
#endif

	fprintf(out.VRMLFile,"] #coordIndex\n");

	fprintf(out.VRMLFile,"} #IndexedFaceSet\n");
	fprintf(out.VRMLFile,"} #Shape\n");
	fprintf(out.VRMLFile,"] } #Transform");
}


/*********************************************************************/
/*********************************************************************/

size_t hashLong( const uint32_t &p )
{
    size_t result = (((size_t)p * 33) ^ (size_t)(p >> 9)) + 1234567L;
    return result;
}

/*********************************************************************/

size_t hashSizeT( const size_t &p )
{
    size_t result = ((p << 25) ^ (p >> 13)) + 0x9e3779b9;
    return result;
}

/*********************************************************************/

// http://isthe.com/chongo/tech/comp/fnv/#public_domain

size_t hashSizeT( const size_t &p, const size_t seed )
{
#if 1
// FNV-like hash modified       badness 0.0002 / 1.99        time 12.15
// reversing parts of p gave worse results
    size_t result = seed ^ (p & 0xFFFFFFFF);
    result = (result << 28) + result * 0x01B3;           // == x<<28 + x*0x1b3 == x<<28 + x<<8 + x*0xb3
    result = result ^ ((p>>32) & 0xFFFFFFFF);
    result = (result << 28) + result * 0x01B3 + 123;    // 113, 127, 131, 157 didn't work as well
#elif 1
// FNV-like hash modified       badness 0.0002 / 1.99        time 12.33
    size_t result = seed ^ (p & 0xFFFFFFFF);
    result = result * 0x100001B3;           // == x<<28 + x*0x1b3 == x<<28 + x<<8 + x*0xb3
    result = result ^ ((p>>32) & 0xFFFFFFFF);
    result = result * 0x100001B3;
#elif 1
// FNV hash        badness 0.0002 / 1.99     time 13.699
    size_t result = seed ^ (p & 0xFF);
    result = result * 0x00000100000001B3;
    result = result ^ ((p>>8) & 0xFF);
    result = result * 0x00000100000001B3;
    result = result ^ ((p>>16) & 0xFF);
    result = result * 0x00000100000001B3;
    result = result ^ ((p>>24) & 0xFF);
    result = result * 0x00000100000001B3;
    result = result ^ ((p>>32) & 0xFF);
    result = result * 0x00000100000001B3;
    result = result ^ ((p>>40) & 0xFF);
    result = result * 0x00000100000001B3;
    result = result ^ ((p>>48) & 0xFF);
    result = result * 0x00000100000001B3;
    result = result ^ ((p>>56) & 0xFF);
    result = result * 0x00000100000001B3;
#else
// simple (but not great) hash          badness  0.001 / 1.99          time 12.07
    size_t result = p + ((seed << 25) ^ (seed >> 13)) + 1234567L;
#endif
    return result;
}

/*********************************************************************/

size_t hashPoint3D( const point3D &p )
{
    uint32_t *longPtr = (uint32_t *)(&p.x);

// using FNV like hash
// 37% empty, badness 0.0001 / 1.99
    size_t result = hashSizeT( longPtr[0], size_t(0xcbf29ce484222325) );
    result = hashSizeT( longPtr[1], result );
    result = hashSizeT( longPtr[2], result );

    return result;
}

/*********************************************************************/

size_t hashPoint3DRounded( const point3D &p )
{
    // these end up around 12-20 bits each
    float tempX = p.x * point3D_places + 0.5f;
    float tempY = p.y * point3D_places + 0.5f;
    float tempZ = p.z * point3D_places + 0.5f;
    size_t longValX( static_cast<size_t>(int64_t(tempX)) );
    size_t longValY( static_cast<size_t>(int64_t(tempY)) );
    size_t longValZ( static_cast<size_t>(int64_t(tempZ)) );

#if 0
// decent, 37% empty   badness 0.002 / 1.99
    size_t result = hashSizeT( hashSizeT( hashSizeT( longValX ) ) ^ hashSizeT( longValY ) ) ^ hashSizeT( longValZ );
#elif 0
// horrible, 65% empty       badness 1.54 / 4.5
    size_t result = std::hash<size_t>{}(longValX);
    result = hashSizeT( std::hash<size_t>{}(longValY), result );
    result = hashSizeT( std::hash<size_t>{}(longValZ), result );
#elif 0
// this should be bad
// badness 0 to 2, mostly around 0.2, 40% empty
    size_t result = (longValX << 40) | (longValY << 20) | longValZ;
// badness 0 to 2, mostly 0.01, still 37% empty
//   result = hashSizeT( result, size_t(0xcbf29ce484222325) );
#else
// using FNV like hash
// 37% empty, badness 0.0001 / 1.99
    size_t result = hashSizeT( longValX, size_t(0xcbf29ce484222325) );
    result = hashSizeT( longValY, result );
    result = hashSizeT( longValZ, result );
#endif

    return result;
}

/*********************************************************************/

point3D quantizePoint3D( const point3D &p )
{
    double XX( p.x * point3D_places );
    double YY( p.y * point3D_places );
    double ZZ( p.z * point3D_places );
    
    XX = floor(XX + 0.5);
    YY = floor(YY + 0.5);
    ZZ = floor(ZZ + 0.5);
    
    point3D result( float(XX)/point3D_places, float(YY)/point3D_places, float(ZZ)/point3D_places );
    return result;
}

/*********************************************************************/

bool EqualPoint3DRounded( const point3D &a, const point3D &b )
{
    int64_t Xa( (int64_t)(a.x * point3D_places + 0.5f) );
    int64_t Ya( (int64_t)(a.y * point3D_places + 0.5f) );
    int64_t Za( (int64_t)(a.z * point3D_places + 0.5f) );
    
    int64_t Xb( (int64_t)(b.x * point3D_places + 0.5f) );
    int64_t Yb( (int64_t)(b.y * point3D_places + 0.5f) );
    int64_t Zb( (int64_t)(b.z * point3D_places + 0.5f) );
    
    return (Xa == Xb) && (Ya == Yb) && (Za == Zb);
}

/*********************************************************************/

// Use unique indices instead of duplicated values.
// Because some 3D packages don't auto-reduce duplicate vertices, and fail to recognize solids without this!
void SaveTriangle( MULTI_3D_FILE &out, const point3D &point1, const point3D &point2, const point3D &point3 )
{

// Using hash table (unordered map) with hash function for floating point values
// this is very generic (no assumptions about data order), and relatively fast, but increases memory usage

// TODO - probably should use a restricted hash map to reduce collisions to a small number, while not increasing memory usage as much
//          Try setting max_load_factor for the map?  That works, but increases time quite a bit.
//            but that will take more code, and more testing
//            And so far memory usage isn't awful until we get to > 10 GB files

    vertexMap::const_iterator the_end = out.vertexLookup.cend();
    
    size_t index1 = 0;
    size_t index2 = 0;
    size_t index4 = 0;
 
    // have to prequantize instead of relying on hash function, becauwse unordered map does an == comparison after hash
    point3D p1 = quantizePoint3D( point1 );
    point3D p2 = quantizePoint3D( point2 );
    point3D p3 = quantizePoint3D( point3 );
    
    // TODO - contains() would be useful, but requires C++20
    vertexMap::const_iterator iter1 = out.vertexLookup.find(p1);
    
    if (iter1 != the_end)
        index1 = (iter1->second);
    else
        {
        index1 = out.vertices.size();
        out.vertexLookup.insert( std::pair<point3D,int>(p1,index1) );
        out.vertices.push_back(p1);
        }
    
    vertexMap::const_iterator iter2 = out.vertexLookup.find(p2);
    
    if (iter2 != the_end)
        index2 = (iter2->second);
    else
        {
        index2 = out.vertices.size();
        out.vertexLookup.insert( std::pair<point3D,int>(p2,index2) );
        out.vertices.push_back(p2);
        }
    
    vertexMap::const_iterator iter3 = out.vertexLookup.find(p3);
    
    if (iter3 != the_end)
        index4 = (iter3->second);
    else
        {
        index4 = out.vertices.size();
        out.vertexLookup.insert( std::pair<point3D,int>(p3,index4) );
        out.vertices.push_back(p3);
        }

    index3 temp( index1, index2, index4 );

    out.faces.push_back( temp );
}

/*********************************************************************/
/*********************************************************************/

void Write3DFlatPolygon( MULTI_3D_FILE &out, const polygon &object, const float Z, const bool top )
{
    //point3D normal( 0.0f, 0.0f, top ? 1.0f : -1.0f );
    
    polygon tempObject = object;
    
    if (!top)
        tempObject = reversePolygon( object );

    // create triangle mesh from arbitrary polygon
    
    // HACK ALERT - this is the simplest algorithm possible: add a point in the middle, which only works for convex polygons
    point2D polyCenter = findPolygonCenter( tempObject );
    
    polygon::const_iterator iter = tempObject.begin();
    polygon::const_iterator prev = iter;
    ++iter;
    
    for (; iter != tempObject.end(); ++iter)
        {
        point3D p1( prev->x, prev->y, Z );
        point3D p2( iter->x, iter->y, Z );
        point3D p3( polyCenter.x, polyCenter.y, Z );
        SaveTriangle( out, p1, p2, p3 );
        prev = iter;
        }
    
    // handle wraparound polys (end, begin)
    {
        iter = tempObject.begin();
        point3D p1( prev->x, prev->y, Z );
        point3D p2( iter->x, iter->y, Z );
        point3D p3( polyCenter.x, polyCenter.y, Z );
        SaveTriangle( out, p1, p2, p3 );
    }

}

/*********************************************************************/

/*
    p1 -- p2
    |\     |
    | \    |
    |  \   |
    |   \  |
    p4 -- p3

    Just write 2 triangles if we know this is a regular quadrilateral
*/
void Write3DFlatRectangle( MULTI_3D_FILE &out, const polygon &object, const float Z, const bool top )
{
    //point3D normal( 0.0f, 0.0f, top ? 1.0f : -1.0f );
    
    polygon tempObject = object;
    
    if (!top)
        tempObject = reversePolygon( object );
    
    point3D p1( tempObject[0].x, tempObject[0].y, Z );
    point3D p2( tempObject[1].x, tempObject[1].y, Z );
    point3D p3( tempObject[2].x, tempObject[2].y, Z );
    point3D p4( tempObject[3].x, tempObject[3].y, Z );
    
#if 0
/* clockwise */
    SaveTriangle( out, p2, p1, p3 );
    SaveTriangle( out, p1, p4, p3 );
#else
/* counter clockwise */
    SaveTriangle( out, p1, p2, p3 );
    SaveTriangle( out, p1, p3, p4 );
#endif
}

/*********************************************************************/
/*********************************************************************/

/*
    p1 -- p2
    |\     |
    | \    |
    |  \   |
    |   \  |
    p4 -- p3

*/

void Write3DConnectingEdges( MULTI_3D_FILE &out, const polygon &previous_poly, const polygon &current_poly, const float bottomZ, const float topZ )
{
    // iterate faces, order is the same on both polygons, rotation is small

    polygon::const_iterator bottom_iter = previous_poly.begin();
    polygon::const_iterator top_iter = current_poly.begin();
    
    point3D p1( top_iter->x, top_iter->y, topZ );
    point3D p4( bottom_iter->x, bottom_iter->y, bottomZ );
    
    point3D topBegin = p1;
    point3D bottomBegin = p4;
    
    ++bottom_iter;
    ++top_iter;
    
    for (; bottom_iter != previous_poly.end(); ++bottom_iter, ++top_iter)
        {
        point3D p2( top_iter->x, top_iter->y, topZ );
        point3D p3( bottom_iter->x, bottom_iter->y, bottomZ );

#if 0
/* clockwise */
        SaveTriangle( out, p1, p2, p3 );
        SaveTriangle( out, p3, p4, p1 );
#else
/* counter clockwise */
        SaveTriangle( out, p2, p1, p3 );
        SaveTriangle( out, p1, p4, p3 );
#endif
        
        p1 = p2;
        p4 = p3;
        }
    
    // handle wraparound polys (end, begin)
#if 0
/* clockwise */
    SaveTriangle( out, p1, topBegin, bottomBegin );
    SaveTriangle( out, bottomBegin, p4, p1 );
#else
/* counter clockwise */
    SaveTriangle( out, topBegin, p1, bottomBegin );
    SaveTriangle( out, p1, p4, bottomBegin );
#endif

}

/*********************************************************************/
/*********************************************************************/

/*
    old      new
          p2
    p1 -/ |
    |\    |
    | \   |
    |  \  p3
    p4 -/
*/

void Write3DConnectingOuterEdges( MULTI_3D_FILE &out, const polygon &previous_poly, const polygon &current_poly, const float bottomZ, const float topZ, const float thickness )
{
    // iterate faces, order is the same on both polygons, rotation is small

    polygon::const_iterator bottom_iter = previous_poly.begin();
    polygon::const_iterator top_iter = current_poly.begin();
    
    for (; bottom_iter != previous_poly.end(); ++bottom_iter, ++top_iter)
        {
        point3D p1( bottom_iter->x, bottom_iter->y, bottomZ+thickness );
        point3D p2( top_iter->x, top_iter->y, topZ+thickness );
        point3D p3( top_iter->x, top_iter->y, topZ );
        point3D p4( bottom_iter->x, bottom_iter->y, bottomZ );

#if 0
/* clockwise */
        SaveTriangle( out, p1, p2, p3 );
        SaveTriangle( out, p3, p4, p1 );
#else
/* counter clockwise */
        SaveTriangle( out, p2, p1, p3 );
        SaveTriangle( out, p1, p4, p3 );
#endif
        }

}

/*********************************************************************/

void Write3DTessellatedRectangle( MULTI_3D_FILE &out, const size_t divisions,
                                    const point3D &base, const point3D &arm1, const point3D &arm2 )
{
    //point3D normal = triangleNormal( base, arm1, arm2 );
    
    point3D delta1 = arm1 - base;
    point3D delta2 = arm2 - base;
    
    double div = divisions;
    
    for (size_t y = 0; y < divisions; ++y)
        {
        for (size_t x = 0; x < divisions; ++x)
            {
            // beyond 20 divisions, this loses precision -- iterating different directions changes the error location
            // but the OBJ file doesn't show the precision loss (only 6 decimal places)
            
            point3D off1 = delta1.scale( (float)((double)x / div) );
            point3D off2 = delta2.scale( (float)((double)y / div) );
            point3D off3 = delta1.scale( (float)((double)(x+1) / div) );
            point3D off4 = delta2.scale( (float)((double)(y+1) / div) );
            point3D p1 = base + off1 + off2;
            point3D p2 = base + off2 + off3;
            point3D p3 = base + off1 + off4;
            point3D p4 = base + off3 + off4;
#if 0
/* clockwise */
            SaveTriangle( out, p2, p1, p3 );
            SaveTriangle( out, p4, p2, p3 );
#else
/* counter clockwise */
            SaveTriangle( out, p1, p2, p3 );
            SaveTriangle( out, p2, p4, p3 );
#endif
            }
        }

}

/*********************************************************************/

/*

STL
https://en.wikipedia.org/wiki/STL_(file_format)
    Well supported in 3D, can write simple triangles
    Binary is less well supported, but 25% the size (still 20% larger than OBJ)

OBJ
https://en.wikipedia.org/wiki/Wavefront_.obj_file
    Trivial, well supported, wants vertices followed by faces

X3D
https://en.wikipedia.org/wiki/X3D
http://www.web3d.org/working-groups/x3d
    XML, Problematic support -- many apps only support a subset
    Can write simple vertex list
    or indexed face set

VRML/WRL - https://en.wikipedia.org/wiki/VRML
    Obsoleted by X3D, but interesting for historical reasons

PLY
https://en.wikipedia.org/wiki/PLY_(file_format)
http://paulbourke.net/dataformats/ply/
    Not many apps supporting this, wants vertices followed by faces

Collada / DAE
https://en.wikipedia.org/wiki/COLLADA
    XML, moderately supported, wants vertices followed by faces
	Really verbose XML

AMF
https://en.wikipedia.org/wiki/Additive_Manufacturing_File_Format
    XML, Not many apps supporting this, wants vertices followed by faces (reduces redundancy)
    Much of what is specified is useless in the real world, and it barely meets the needs of current printers
	Excessively verbose repetitive and redundant XML


NOPE - U3D -- https://en.wikipedia.org/wiki/Universal_3D
http://www.ecma-international.org/publications/standards/Ecma-363.htm
    Documentation is somewhere between Crap and Barf!

HARD NO - 3MF -- https://en.wikipedia.org/wiki/3D_Manufacturing_Format
	Not well supported outside Microsoft and Autodesk
	More or less a print ticket container with insane structure
	Excessively verbose repetitive and redundant XML model description
	Looks like it uses file system directories in some cases, or uses PKZIP for single file archive
	Looks a lot like Microsoft's attempt to destroy the 3D printing market

*/

MULTI_3D_FILE Start3DFile( const char *base_testname, const size_t faces_hint )
{
    MULTI_3D_FILE result;
    
    const int string_limit = 1024;
    char filename[string_limit];
    
    if ( strlen(base_testname) > (string_limit-5) )
        {
        fprintf(stderr,"filename %s is too long for internal string limits\n", base_testname );
        return result;
        }

    if (outputSTLFile)
        {
        snprintf( filename, string_limit, "%s_ascii.stl", base_testname );

        result.STLFile = fopen( filename, "w" );
        
        if (!result.STLFile)
            {
            fprintf(stderr,"WARNING - could not create file %s\n", filename );
            }
        else
            WriteSTLHeader( result.STLFile );
        }

    if (outputSTLBinaryFile)
        {
        snprintf( filename, string_limit, "%s_binary.stl", base_testname );

        result.STLBinaryFile = fopen( filename, "wb" );
        
        if (!result.STLBinaryFile)
            {
            fprintf(stderr,"WARNING - could not create file %s\n", filename );
            }
        else
            WriteSTLBinaryHeader( result.STLBinaryFile );
        }

    if (outputX3DFile)
        {
        snprintf( filename, string_limit, "%s.x3d", base_testname );
        
        result.X3DFile = fopen( filename, "w" );
        
        if (!result.X3DFile)
            {
            fprintf(stderr,"WARNING - could not create file %s\n", filename );
            }
        else
            WriteX3DHeader( result.X3DFile );
        }

    if (outputX3DIndexFile)
        {
        snprintf( filename, string_limit, "%s_index.x3d", base_testname );
        
        result.X3DIndexFile = fopen( filename, "w" );
        
        if (!result.X3DIndexFile)
            {
            fprintf(stderr,"WARNING - could not create file %s\n", filename );
            }
        else
            WriteX3DIndexHeader( result.X3DIndexFile );
        }

    if (outputPLYFile)
        {
        snprintf( filename, string_limit, "%s.ply", base_testname );
        
        result.PLYFile = fopen( filename, "w" );
        
        if (!result.PLYFile)
            {
            fprintf(stderr,"WARNING - could not create file %s\n", filename );
            }
        else
            WritePLYHeader( result.PLYFile );
        }

    if (outputPLYBinaryFile)
        {
        snprintf( filename, string_limit, "%s_binary.ply", base_testname );
        
        result.PLYBinaryFile = fopen( filename, "w" );
        
        if (!result.PLYBinaryFile)
            {
            fprintf(stderr,"WARNING - could not create file %s\n", filename );
            }
        else
            WritePLYBinaryHeader( result.PLYBinaryFile );
        }

    if (outputAMFFile)
        {
        snprintf( filename, string_limit, "%s.amf", base_testname );
        
        result.AMFFile = fopen( filename, "w" );
        
        if (!result.AMFFile)
            {
            fprintf(stderr,"WARNING - could not create file %s\n", filename );
            }
        else
            WriteAMFHeader( result.AMFFile );
        }

    if (outputOBJFile)
        {
        snprintf( filename, string_limit, "%s.obj", base_testname );
        
        result.OBJFile = fopen( filename, "w" );
        
        if (!result.OBJFile)
            {
            fprintf(stderr,"WARNING - could not create file %s\n", filename );
            }
        else
            WriteOBJHeader( result.OBJFile );
        }

    if (outputDAEFile)
        {
        snprintf( filename, string_limit, "%s.dae", base_testname );
        
        result.DAEFile = fopen( filename, "w" );
        
        if (!result.DAEFile)
            {
            fprintf(stderr,"WARNING - could not create file %s\n", filename );
            }
        else
            WriteDAEHeader( result.DAEFile );
        }

    if (outputVRMLFile)
        {
        snprintf( filename, string_limit,"%s.wrl", base_testname );
        
        result.VRMLFile = fopen( filename, "w" );
        
        if (!result.VRMLFile)
            {
            fprintf(stderr,"WARNING - could not create file %s\n", filename );
            }
        else
            WriteVRMLHeader( result.VRMLFile );
        }

    // if we're storing data, then reserve approximate space up front to save time
    if ( faces_hint != 0 )
        {
        result.faces.reserve(faces_hint);
        size_t vertex_estimate = std::max( size_t(8), (faces_hint * 2) / 3 );  // usually overestimating
        result.vertices.reserve(vertex_estimate);

#if 1
/*
max_load_factor - probably want to increase whenever size is too large, to reduce memory usage
    But what is TOO large?
0.7         time 11.75
0.8         time 11.84
0.9         time 12.05
1.0         time 12.15
1.2         time 12.76
1.5         time 13.18
2.0         time 14.09
3.0         time 15.70
5.0         time 17.41
10.0        time 22.34
20.0        time 30.94
 */
        float mlf = 1.0;
        if (vertex_estimate > 1000000000L )     // 1 billion
            mlf = 10.0;
        else if (vertex_estimate > 20000000L )  // 20 million
            mlf = 2.0;
        result.vertexLookup.max_load_factor( mlf );
#endif

        // too large or too small: both end up slower
        // but a small increment improves the worst case
        result.vertexLookup.reserve( vertex_estimate + 23 );
        }
 
    result.faces_hint = faces_hint;
    
    return result;
}

/*********************************************************************/

void Finish3DFile( MULTI_3D_FILE &out )
{
    if (out.STLFile)
        {
        WriteSTLFooter( out );
        fclose(out.STLFile);
        out.STLFile = NULL;
        }
    
    if (out.STLBinaryFile)
        {
        WriteSTLBinaryFooter( out );
        fclose(out.STLBinaryFile);
        out.STLBinaryFile = NULL;
        }

    if (out.X3DFile)
        {
        WriteX3DFooter( out );
        fclose(out.X3DFile);
        out.X3DFile = NULL;
        }

    if (out.X3DIndexFile)
        {
        WriteX3DIndexFooter( out );
        fclose(out.X3DIndexFile);
        out.X3DIndexFile = NULL;
        }

    if (out.PLYFile)
        {
        WritePLYFooter( out );
        fclose(out.PLYFile);
        out.PLYFile = NULL;
        }

    if (out.PLYBinaryFile)
        {
        WritePLYBinaryFooter( out );
        fclose(out.PLYBinaryFile);
        out.PLYBinaryFile = NULL;
        }

    if (out.AMFFile)
        {
        WriteAMFFooter( out );
        fclose(out.AMFFile);
        out.AMFFile = NULL;
        }

    if (out.OBJFile)
        {
        WriteOBJFooter( out );
        fclose(out.OBJFile);
        out.OBJFile = NULL;
        }

    if (out.DAEFile)
        {
        WriteDAEFooter( out );
        fclose(out.DAEFile);
        out.DAEFile = NULL;
        }

    if (out.VRMLFile)
        {
        WriteVRMLFooter( out );
        fclose(out.VRMLFile);
        out.VRMLFile = NULL;
        }
}

/*********************************************************************/


#define DEBUG_CONTAINERS    0

MULTI_3D_FILE::~MULTI_3D_FILE()
{

#if DEBUG_CONTAINERS
    // debugging/tuning stats for containers
    
    // looks like the vector hash is working ok, but most cases have 37% of buckets empty!
    size_t bucket_count = vertexLookup.bucket_count();
    size_t max_bucket = 0;
    size_t empties = 0;
    size_t cost = 0;    // cost to search for each key == number of linked items checked to reach each key
    for (size_t i = 0; i < bucket_count; ++i) {
        size_t this_size = vertexLookup.bucket_size(i);
        if (this_size == 0)
            empties++;
        else {
            max_bucket = std::max( max_bucket, this_size );
            cost += this_size * this_size;  // N items, N steps to search linked list
        }
    }
    
    double lambda = double(vertexLookup.size()) / double(bucket_count);   // == load_factor, range 0 to 1 in implementation, but could be higher due to memory limits
    double cost_fraction = double(cost) / double(vertexLookup.size());  // == average cost of lookup, range 1.0 to infinity; typically between 1 and 2;  1 is good, over 2 is bad
    double temp = ( cost_fraction / (1.0 + lambda) ) - 1.0;
    //double temp2 = (cost_fraction - 1.0) / (1.0 + lambda);
    double badness = std::max(0.0, temp);
    
    double empty_fraction = double(empties) / bucket_count;
    
    printf("Multi3DFile hint:%lu, face:%lu/%lu, vertex:%lu/%lu, lookup:%lu/%lu/%f/%lu/%f badness: %f/%f\n",
        faces_hint,
        faces.size(), faces.capacity(),
        vertices.size(), vertices.capacity(),
        vertexLookup.size(), vertexLookup.bucket_count(),
        vertexLookup.load_factor(), max_bucket, empty_fraction, badness, cost_fraction );
#endif

}

/*********************************************************************/
/*********************************************************************/
    

