//
//  geometry.hh
//
//  Copyright © 2016-2023 Chris Cox. All rights reserved.
//  Distributed under the MIT License
//

#ifndef GEOMETRY_HH
#define GEOMETRY_HH

#include <vector>
#include "config.h"
#include "utilities.hh"

/*********************************************************************/

typedef struct point2D {

    point2D( float xx, float yy )
        { x = xx; y = yy; }
    
    point2D()
        { x = y = 0.0f; }
    
    float x, y;

} point2D;

/*********************************************************************/

const int32_t point3D_places = 100000;

struct point3D;
bool EqualPoint3DRounded( const point3D &a, const point3D &b );

typedef struct point3D {

    point3D( float xx, float yy, float zz )
        { x = xx; y = yy; z = zz; }
    
    point3D()
        { x = y = z = 0.0f; }
    
    bool operator==(const point3D &other) const
        {
        return (other.x == x) && (other.y == y) && (other.z == z);
        }
    
    point3D operator+(const point3D &other) const
        {
        point3D result( x+other.x, y+other.y, z+other.z);
        return result;
        }
    
    point3D operator-(const point3D &other) const
        {
        point3D result( x-other.x, y-other.y, z-other.z);
        return result;
        }
    
    point3D scale(float s) const
        {
        point3D result( x*s, y*s, z*s);
        return result;
        }
    
    float x, y, z;

} point3D;

/*********************************************************************/

typedef struct index3 {

    index3( uint64_t xx, uint64_t yy, uint64_t zz )
        { x = xx; y = yy; z = zz; }
    
    index3()
        { x = y = z = 0; }
    
    uint64_t x, y, z;

} index3;

/*********************************************************************/

typedef struct triangle {

    triangle( const point3D &i1, const point3D &i2, const point3D &i3 )
        { p1 = i1; p2 = i2; p3 = i3; }
    
    bool operator==(const triangle &other) const
        {
        return (other.p1 == p1) && (other.p2 == p2) && (other.p3 == p3);
        }

    point3D    p1, p2, p3;

} triangle;

/*********************************************************************/

typedef std::vector<point2D> polygon;        // counter clockwise point order

typedef std::vector<point3D> polygon3D;

typedef std::vector<float> floatList;

typedef std::vector<triangle> triangle_list;

/*********************************************************************/

#endif // GEOMETRY_HH
