//
//  geometry.cpp
//
//  Copyright © 2016-2022 Chris Cox. All rights reserved.
//  Distributed under the MIT License
//

#include <cmath>
#include <climits>
#include "geometry.h"

/*********************************************************************/

polygon rotatePolygon( const polygon &input, const float angle )
{
    float ca = cosf( angle );
    float sa = sinf( angle );
    
    polygon result;
    result.reserve( input.size() );
    
    for (polygon::const_iterator iter = input.begin(); iter != input.end(); ++iter)
        {
        point2D temp = rotatePoint( *iter, ca, sa );
        result.push_back( temp );
        }
    
    return result;
}

/*********************************************************************/

polygon createPolygonVectors( const polygon &input )
{
    size_t    count = input.size();

    polygon result;
    result.resize( count );
    
    for (size_t i = 0; i < count; ++i)
        {
        size_t nextIndex = i+1;
        if (i >= (count-1))            // wraparound for last entry
            nextIndex = 0;
        point2D temp = subtractPoints( input[nextIndex], input[i] );    // 1 - 0, origin is always points[i]
        point2D tempN = normalizeVector( temp );
        result[i] = tempN;
        }
    
    return result;
}

/*********************************************************************/

floatList createPolygonLengths( const polygon &input )
{
    size_t    count = input.size();

    floatList result;
    result.resize( count );
    
    for (size_t i = 0; i < count; ++i)
        {
        size_t nextIndex = i+1;
        if (i >= (count-1))            // wraparound for last entry
            nextIndex = 0;
        point2D temp = subtractPoints( input[nextIndex], input[i] );    // 1 - 0, origin is always points[i]
        float dist = hypotf( temp.x, temp.y );
        result[i] = dist;
        }
    
    return result;
}

/*********************************************************************/

polygon offsetPolygon( const polygon &input, const point2D &offset )
{
    polygon result;
    result.reserve( input.size() );
    
    for (polygon::const_iterator iter = input.begin(); iter != input.end(); ++iter)
        {
        point2D temp = offsetPoint( *iter, offset );
        result.push_back( temp );
        }
    
    return result;
}

/*********************************************************************/

void boundsPolygon( const polygon &input, point2D &min, point2D &max )
{
    float min_x = 99999;
    float max_x = -99999;
    float min_y = 99999;
    float max_y = -99999;
    
    for (polygon::const_iterator iter = input.begin(); iter != input.end(); ++iter)
        {
        float xx = iter->x;
        float yy = iter->y;
        if (xx < min_x)        min_x = xx;
        if (xx > max_x)        max_x = xx;
        if (yy < min_y)        min_y = yy;
        if (yy > max_y)        max_y = yy;
        }
    
    min = point2D( min_x, min_y );
    max = point2D( max_x, max_y );
    }

/*********************************************************************/

polygon scalePolygon( const polygon &input, const float scale )
{
    polygon result;
    result.reserve( input.size() );
    
    for (polygon::const_iterator iter = input.begin(); iter != input.end(); ++iter)
        {
        point2D temp = scalePoint( *iter, scale );
        result.push_back( temp );
        }
    
    return result;
}

/*********************************************************************/

polygon scaleToFitPolygon( const polygon &input, const float maxValue )
{
    point2D min, max;
    boundsPolygon( input, min, max );
    point2D bounds( (max.x - min.x), (max.y - min.y) );
    float maxLen = std::max( bounds.x, bounds.y );
    float scale = maxValue / maxLen;
    polygon result = scalePolygon( input, scale );
    return result;
}

/*********************************************************************/

float findMaxRadius( const polygon &input )
{
    size_t    count = input.size();
    float result = 0.0f;
    
    for (size_t i = 0; i < count; ++i)
        {
        float dist = hypotf( input[i].x, input[i].y );
        result = std::max( dist, result );
        }
    
    return result;
}

/*********************************************************************/

polygon reversePolygon( const polygon &input )
{
    polygon result = input;
    
    std::reverse( result.begin(), result.end() );
    
    return result;
}

/*********************************************************************/

point3D triangleNormal( const point3D &p1, const point3D &p2, const point3D &p3 )
{
    point3D U = subtractPoints( p2, p1 );
    point3D V = subtractPoints( p3, p1 );
    
    float xx = U.y * V.z - U.z * V.y;
    float yy = U.z * V.x - U.x * V.z;
    float zz = U.x * V.y - U.y * V.x;
    
    point3D s( xx, yy, zz );
    point3D result = normalizeVector( s );

    return result;
}

/*********************************************************************/

triangle_list scale3D( triangle_list triangles, float scale )
{
    triangle_list result = triangles;
    
    for (triangle_list::iterator iter = result.begin(); iter != result.end(); ++iter)
        {
        iter->p1 = scalePoint( iter->p1, scale );
        iter->p2 = scalePoint( iter->p2, scale );
        iter->p3 = scalePoint( iter->p3, scale );
        }
    
    return result;
}

/*********************************************************************/

triangle_list offset3D( triangle_list triangles, point3D offset )
{
    triangle_list result = triangles;
    
    for (triangle_list::iterator iter = result.begin(); iter != result.end(); ++iter)
        {
        iter->p1 = offsetPoint( iter->p1, offset );
        iter->p2 = offsetPoint( iter->p2, offset );
        iter->p3 = offsetPoint( iter->p3, offset );
        }
    
    return result;
}

/*********************************************************************/

point3D Jitter3D( const float jitter )
{
    long tempX = random() & 0x0FFFF;    // range 0..65535
    long tempY = random() & 0x0FFFF;
    long tempZ = random() & 0x0FFFF;

    float x = jitter * (tempX - 32767) / 32767.0f;
    float y = jitter * (tempY - 32767) / 32767.0f;
    float z = jitter * (tempZ - 32767) / 32767.0f;

    point3D result( x, y, z );
    return result;
}

/*********************************************************************/
/*********************************************************************/
